<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Store;
use App\Util\StoreImport;
use Maatwebsite\Excel\Facades\Excel;

/**
 * 產生店家代碼
 * @author Min
 *
 */
class GenStoreCodeCmd extends Command
{
    
    private $del_count =0;
    private $update_count =0;
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'store:import';

     
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '產生店家代碼';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
     
    
    function rand(){
       
        $randArr = array();
        for($i = 0; $i < 2; $i++){
            $randArr[$i] = rand(0, 9);
            $randArr[$i +2] = chr(rand(0, 25) + 65);
        }
        //打亂排序
        shuffle($randArr);
        return implode('', $randArr);
    }
    
  
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        echo ("Geb Store code start \n");
        Excel::import(new StoreImport, 'store.xlsx');
    }
}

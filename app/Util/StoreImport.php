<?php
namespace App\Util;


use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use App\Models\Store;

class StoreImport implements ToCollection, WithStartRow
{
    use Importable;
    
    function rand(){
        
        $randArr = array();
        for($i = 0; $i < 2; $i++){
            $randArr[$i] = rand(0, 9);
            $randArr[$i +2] = chr(rand(0, 25) + 65);
        }
        //打亂排序
        shuffle($randArr);
        return implode('', $randArr);
    }
    
    public function collection(Collection $rows)
    {
        $insert = 0;
        $update =0;
        
        foreach ($rows as $row)
        {
            $n=0;
            
            $store_db = Store::where("store_no",$row[0])->first();
            
            if ($store_db) {
                if ($store_db->fma  != $row[9]) {
                    $update++;
                    $store_db->fma = $row[9]; //已存在更新FMA
                    $store_db->save();
                }
            } else {
                $insert++;
                
                $code ="";
                while (true) {
                    $code = $this->rand();
                     
                    if (!Store::where("store_code" ,$code)->exists()) {
                        break;
                    }
                   
                }
                
                $store = new Store();
                $store->store_no = $row[$n++];
                $store->name = $row[$n++];
                $store->area_name = $row[$n++];
                $store->addr = $row[$n++];
                $store->addr_descript = $row[$n++];
                $store->area = $row[$n++];
                $store->path_partner_no = $row[$n++];
                $store->jti_area_no = $row[$n++];
                $store->sales_area_no = $row[$n++];
                $store->fma = $row[$n++];
                $store->store_code =$code;
                $store->save();
            }
        }
        
        echo "data size:".count($rows)." insert :$insert update:$update";
        echo "<br>";
    }
    
    
    
    public function startRow(): int
    {
        return 2;
    }
}
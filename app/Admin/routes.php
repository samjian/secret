<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->resource('/campaign', 'CampaignController');
    
    $router->get('/answer/checkAnswer', 'AnswerController@checkAnswer');
    $router->get('/answer/{id}/pass', 'AnswerController@pass');
    $router->get('/answer/{id}/nopass', 'AnswerController@nopass');
    $router->get('/answer/{id}/approved', 'AnswerController@approved');
   
    
  
  //  $router->get('/summary', 'AnswerController@summary');
    
    $router->resource('/answer', 'AnswerController');
    
    $router->get('/getStore/{store_code}', 'StoreController@getStore');
   
    
    $router->get('/chart', 'ChartController@index');
    $router->get('/chart/detail', 'ChartController@detail');
    
    $router->get('/analyze', 'AnalyzeController@index');
    $router->get('/month', 'MonthController@index');
    
    $router->get('/summary/export', 'SummaryController@export');
    $router->resource('/summary', 'SummaryController');
    
    
   // $router->get('/chart/query', 'ChartController@query');
});


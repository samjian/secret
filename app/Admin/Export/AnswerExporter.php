<?php
namespace App\Admin\Export;

 

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

use App\Models\Answer;
use App\Models\Campaign;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class AnswerExporter implements  WithMapping, WithHeadings , FromCollection , ShouldAutoSize,WithColumnWidths, WithStyles
{
    
    
    public $campaign;
    public $status;
    public $area;
    
    public function __construct($campaignId, $status = null, $area= null){
        $this->campaign  = Campaign::find($campaignId);
        $this->status = $status;
        $this->area = $area;
    }
    
    
    public function collection()
    {
        $rows = null;
        $answer =  Answer::where("campaign_id",$this->campaign->id);
      
        if ($this->status == null) {
              $answer->WhereIn("status", [1,2,3]);
        } else {
              $answer->where("status", $this->status);
        }
        
        if ($this->area && $this->area != "全國" ) {
            $answer->join('stores', 'stores.store_code', '=', 'answers.store_code');
            $answer->where("stores.area", $this->area);
        }
        
        $rows =$answer->get();
        return $rows;
    }
    
    public function styles(Worksheet $sheet)
    {
        $sheet->getStyle('O')->getAlignment()->setWrapText(true);
        $sheet->getStyle('R')->getAlignment()->setWrapText(true);
        $sheet->getStyle('U')->getAlignment()->setWrapText(true);
        $sheet->getStyle('X')->getAlignment()->setWrapText(true);
        
        $sheet->getStyle('N')->getAlignment()->setWrapText(true);
        $sheet->getStyle('Q')->getAlignment()->setWrapText(true);
        $sheet->getStyle('T')->getAlignment()->setWrapText(true);
    }
    
    public function headings(): array
    {
        return [
            "通路夥伴代碼",
            "JTI分區",
            "業代區域",
            "FMA",
            "客戶店號",	
            "店名",
            "地址",
            "地址備註",
            "地址補充",
            "店訪日期",
            "店訪狀態",
   	        "店訪狀態說明",
            "直接淘汰題:",
            "題目一：". $this->campaign->topic_1,
            "店家回應菸品",
            "店家回應其他",
            "題目二：".$this->campaign->topic_2,
            "店家回應菸品",
            "店家回應其他",
            "題目三提問：".$this->campaign->topic_3,
            "店家回應菸品",
            "店家回應其他",
            "題目四提問：".$this->campaign->topic_4,
            "店家回應菸品",
            "店家回應其他",
          	"開放回應題目一:"	,
            "開放回應題目二:",
            "買回菸品價錢",
            "買回菸品條碼",
            "未買回菸品原因",
            "禮卷類別",
            "發放的禮卷金額",
            "查核成績",
            "審核狀態",
        ];
    }
    
 
    public function map($row) : array
    {
        $products1 = implode(",",$row->products1->map(function ($item,$key) {
            return $item->name ." ".$item->describe;
        })->all());
        
        $products1 = str_replace(  "," , "\r", $products1);
         
            
        $products2 = implode(",",$row->products3->map(function ($item,$key) {
            return $item->name ." ".$item->describe;
        })->all());
        
        $products2 = str_replace(  "," , "\r", $products2);
        
        $products3 = implode(",",$row->products3->map(function ($item,$key) {
            return $item->name ." ".$item->describe;
        })->all());
        
        $products3 = str_replace(  "," , "\r", $products3);
        
        $products4 = implode(",",$row->products4->map(function ($item,$key) {
            return $item->name ." ".$item->describe;
        })->all());
            
        $products4 = str_replace(  "," , "\r", $products3);
        
        /*
            "開放回應題目一:"	,
            "開放回應題目二:",
            "買回菸品價錢",
            "買回菸品條碼",
            "未買回菸品原因",
            "發放的禮卷金額",
            "查核成績".
            "審核狀態",
        */
        
       
        
        return [
            $row->store->path_partner_no,
            $row->store->area,
            $row->store->sales_area_no,
            $row->user->name,
            $row->store->store_no,
            $row->store->name,
            $row->store->area_name ." ". $row->store->addr ,
            $row->store->addr_descript,
            $row->addr_memo,
            $row->access_date,
            Answer::ACCESS_STATUS[$row->access_status],
            $row->access_memo,
            $row->topic_0_answers,
            implode("\r", $row->topic_1_answers),
            $products1,
            $row->topic_1_answers_other,
            implode("\r", $row->topic_2_answers),
            $products2,
            $row->topic_2_answers_other,
            implode("\r", $row->topic_3_answers),
            $products3,
            $row->topic_3_answers_other,
            $row->topic_4_answers,
            $products4,
            $row->topic_4_answers_other,
            $row->topic_open_1_answers,
            $row->topic_open_2_answers,
          
            $row->tobacco_price,
            $row->tobacco_code." ",
            $row->tobacco_reason,
            Campaign::VOUCHAR_ALL_TYPE[$this->campaign->voucher_type],
            $row->voucher,
            $row->score,
            Answer:: STATUS[$row->status],
        ];
    } 
    
    public function columnWidths(): array
    {
        return [
            'A' => 15,
            'B' => 10,
            'C' => 15,
            'D' => 15,
            'E' => 15,
            'F' => 15,
            'G' => 30,
            'H' => 25,
            'I' => 15,
            'J' => 20,
            'K' => 20,
            
            'L' => 15,
            'M' => 40,
            'N' => 40,
            'O' => 40,
            'P' => 40,
            'Q' => 40,
            "R"=> 40,
            "S"=> 40,
            "T"=> 40,
            "U"=> 40,
            "V"=> 40,
            "W"=> 40,
            "X"=> 40,
            "Y"=> 40,
            "Z"=> 40,
            'AA' => 15,
            'AB' => 15,
            'AC' => 15,
            'AD' => 15,
        ];
    }
    
}


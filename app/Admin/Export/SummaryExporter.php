<?php
namespace App\Admin\Export;

 
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

use Illuminate\Support\Facades\DB;

use App\Models\Campaign;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithMapping;



class SummaryExporter    implements    WithHeadings , FromArray , ShouldAutoSize , WithColumnWidths
{
    public function __construct(){
      
       
    }
    
    
    public function array(): array
    {
        
        $rows = null;
        
        $name = request()->name;
      
        $no= $this->removeArrNull(request()->no);
        $cid= $this->removeArrNull(request()->cid);
        
        $area_tyep = request()->area_tyep;
        $startdate = null;
        $enddate = null;
         
        if(request()->date) {
            $startdate = request()->date["start"];
            $enddate = request()->date["end"];
        }
        
        $where="";
        $whereData=[];
        /*
        if (request()->area_tyep ==0 || $whereData == 0 ) {
            $where .= "and c.id = (select id   from  campaigns  where startdate < now() order by startdate  desc LIMIT 1)";
        } else {
            $where .= " and  s.area  = ?";
            $whereData[] =  Campaign::AREA[request()->area_tyep];
        } */
        
        if(request()->area_tyep > 0){
            $where .= " and  s.area  = ?";
            
            $whereData[] =  Campaign::AREA[request()->area_tyep];
        }
        
        
        
        if (sizeof($no) > 0) {
            $str = implode("," , $no);
            $where .= " and c.no in ($str)";
        }
        
       
        if (sizeof($cid) > 0) {
            $str = implode("," , $cid);
            $where .= " and c.id in ($str)";
            
            
        }
        $sub_where ="";
         
        if ($startdate) {
            $where .= " and a.access_date >=  ?";
            $whereData[] = $startdate;
            
            $sub_where =" and  sub_a.access_date  >= '$startdate'";
        }
        
        if ($enddate) {
            $where .= " and  ? <=  a.access_date";
            $sub_where .= " and  sub_a.access_date <= '$enddate'";
            $whereData[] =$enddate;
        }
        
        
        $sql = "
            select
            s.area ,
            s.path_partner_no ,
            
            count(a.store_code)  store_count,
            (
                select count(1) from  answers sub_a, stores sub_s
                where sub_a.store_code = sub_s.store_code and sub_a.access_status=2 and sub_s.path_partner_no = s.path_partner_no
                $sub_where
            ) as two_access ,
            (
            select count(1) from  answers sub_a, stores sub_s
            where sub_a.store_code = sub_s.store_code and sub_s.path_partner_no = s.path_partner_no
            and sub_a.topic_1_answers_score > 0  and sub_a.access_status in(0,1) 
            $sub_where
            ) topic_1_score_count,
            (
            select count(1) from  answers sub_a, stores sub_s
            where sub_a.store_code = sub_s.store_code and sub_s.path_partner_no = s.path_partner_no
            and sub_a.topic_2_answers_score > 0  and sub_a.access_status in(0,1)
            $sub_where 
            ) topic_2_score_count,
            (
            select count(1) from  answers sub_a, stores sub_s
            where sub_a.store_code = sub_s.store_code and sub_s.path_partner_no = s.path_partner_no
            and sub_a.topic_3_answers_score > 0  and sub_a.access_status in(0,1) 
            $sub_where
            ) topic_3_score_count, 
            
            CONCAT(FORMAT((
            select count(1) from campaigns sub_c,  answers sub_a, stores sub_s
            where sub_c.id = sub_a.campaign_id and sub_a.store_code = sub_s.store_code and sub_s.path_partner_no = s.path_partner_no
            and sub_a.score > sub_c.score  and sub_a.access_status in(0,1) 
            $sub_where
            )  / count(1)  * 100 ,2), '%')  as pass
            
            from campaigns c , answers a , stores s
            where c.id = a.campaign_id
            and s.store_code = a.store_code
            and a.status =2
            
            $where
            
            GROUP BY  c.id,c.no,c.name,s.area , s.path_partner_no 
            Order by    c.startdate desc ,  pass desc 
             
        ";
           
        $rows = DB::select($sql, $whereData) ;
       
        return $rows;
    }
    
   
    public function headings(): array
    {
        return [
                "分區",
                "通路夥伴",
            	"有效查核店家數",
                "未營業",
                "第一層提問成功家數（得分>0店家)",
                "第二層提問成功家數（得分>0店家）",
                "第三層提問成功家數（得分>0店家）",
                "合格率",
        ];
    }
    
  
   
    
    public function columnWidths(): array
    {
        return [
            'A' => 20,
            'B' => 20,
            'C' => 20,
            'D' => 20,
            'E' => 35,  
            'F' => 35,
            'G' => 35,  
            'H' => 30,
        ];
    }
    

   
    public function removeArrNull($arr) {
        if (!$arr) return [];
        
        $n =0;
        foreach ($arr as $val) {
            if ($val ==null) {
                unset($arr[$n]);
                break;
            }
            $n++;
        }
        
        return $arr;
    }
    
}


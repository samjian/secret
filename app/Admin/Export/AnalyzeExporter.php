<?php
namespace App\Admin\Export;

 

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

use App\Models\Answer;
use App\Models\Campaign;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Illuminate\Http\Request;

class AnalyzeExporter implements  WithMapping, WithHeadings , FromCollection , ShouldAutoSize,WithColumnWidths, WithStyles
{
    
    public $campaign;
   
    public function __construct(){
        $campaign_id = [];
        
        if (request()->campaign_id) {
            $campaign_id= $this->removeArrNull(request()->campaign_id);
        } else {
            $campaign_id= $this->removeArrNull(request()->cid);
        }
        
        $is_title = request()->file_name == "明細報表";
        
        if ($is_title) {
            if (sizeof($campaign_id) > 0) {
                $cid = $campaign_id[0];
                $this->campaign = Campaign::find($cid);
            } else {
                $this->campaign = Campaign::whereRaw("campaigns.id = (SELECT campaign_id from answers where status =2 and  access_status in(0,1)  order by access_date desc LIMIT  1)")->first();
               
            }
        }
     
    }
   
    public function removeArrNull($arr) {
        if (!$arr ) return [];
       
        if (is_array($arr)) {
            $n =0;
            foreach ($arr as $val) {
                if ($val ==null) {
                    unset($arr[$n]);
                    break;
                }
                $n++;
            }
            return $arr;
        } else {
            return [$arr];
        }
        
    }
    
    public function collection()
    { 
        $jti_area_no =$this->removeArrNull( request()->jti_area_no) ;
        $path_partner_no= $this->removeArrNull(request()->path_partner_no);
        $sales_area_no=$this->removeArrNull( request()->sales_area_no);
        $store_code= $this->removeArrNull(request()->store_code);
        $campaign_id = [];
       
        $cid= $this->removeArrNull(request()->cid);
        $startdate= $this->removeArrNull(request()->startdate);
        $enddate= $this->removeArrNull(request()->enddate);
        
      
        
        if (request()->campaign_id) {
            $campaign_id= $this->removeArrNull(request()->campaign_id);
        } else {
            $campaign_id= $this->removeArrNull(request()->cid);
        }
        
      
        
        
        $rows = null;
       
        $answer =  Answer::select('answers.score as answer_score','answers.*', 'campaigns.*', 'stores.*');
       
        $answer->join('campaigns', 'campaigns.id', '=', 'answers.campaign_id');
        $answer->join('stores', 'stores.store_code', '=', 'answers.store_code');
        
        $answer->where("answers.status" , 2);
        $answer->whereIn("answers.access_status" , [0,1]);
      
        
        $answer->orderByRaw("field(stores.area ,'北一區', '北二區','中區','南區') asc, campaigns.startdate desc , answer_score desc" );
         
        if (request()->type ==1) { //合格
            $answer->whereRaw("answers.score >= campaigns.score");
        } else if (request()->type ==2) {//不合格
            $answer->whereRaw("answers.score < campaigns.score");
        }
        
        if (sizeof($path_partner_no) >0 ) {
           
        }
       
        
        if (sizeof($path_partner_no) >0 ) {
            $answer->whereIn("stores.path_partner_no",   $path_partner_no);
        } else {
            
            
            if(request()->area){
                
                $aeraSql ="";
                
                if (request()->area !="全國") {
                    $answer->where("stores.area",  request()->area);
                } else {
                    if (sizeof($cid) == 0)  {
                        $answer->whereRaw("campaigns.id = (SELECT campaign_id from answers where status =2 and  access_status in(0,1)  order by access_date desc LIMIT  1)");
                    }
                }
            }
        }
        
        
        if (sizeof($sales_area_no) > 0) {
            $answer->whereIn("stores.sales_area_no",   $sales_area_no);
        }
        
        if (sizeof($store_code) >0) {
            $answer->whereIn("stores.store_code",   $store_code);
        }
        
        if (sizeof($campaign_id) > 0) {
            $answer->whereIn("campaigns.id",   $campaign_id);
        }
        
        
        if(sizeof($jti_area_no) >0 ){
            $answer->whereIn("stores.jti_area_no", $jti_area_no);
        }
        
        
        if ($startdate) {
            $answer->where("answers.access_date", ">=" , request()->startdate);
        }
        
        if ($enddate) {
            $answer->where("answers.access_date", "<=" , request()->enddate);
        }
        
      
        $rows =$answer->get();
        
       
        
        return $rows;
    }
    
    public function styles(Worksheet $sheet)
    {
       
        $sheet->getStyle('O')->getAlignment()->setWrapText(true);
        $sheet->getStyle('R')->getAlignment()->setWrapText(true);
        $sheet->getStyle('U')->getAlignment()->setWrapText(true);
        $sheet->getStyle('X')->getAlignment()->setWrapText(true);
        
        $sheet->getStyle('N')->getAlignment()->setWrapText(true);
        $sheet->getStyle('Q')->getAlignment()->setWrapText(true);
        $sheet->getStyle('T')->getAlignment()->setWrapText(true);
    }
    
    public function headings(): array
    {
        $is_title = request()->file_name == "明細報表";
      
        return [
            "分區",
            "TP",
            "FMA",
            "TPSR",
            "客戶店號",	
            "店名",
            "地址",
            "地址備註",
            "地址補充",
            "店訪日期",
            "店訪狀態",
   	        "店訪狀態說明",
            "直接淘汰題:",
            "題目一： ".  ($is_title ? $this->campaign->topic_1 :""),
            "店家回應菸品",
            "店家回應其他",
            "題目二： ".  ($is_title ? $this->campaign->topic_2 : ""),
            "店家回應菸品",
            "店家回應其他",
            "題目三提問： ".  ($is_title ? $this->campaign->topic_3 :""),
            "店家回應菸品",
            "店家回應其他",
            "題目四提問： ".  ($is_title ?$this->campaign->topic_4 :""),
            "店家回應菸品",
            "店家回應其他",
          	"開放回應題目一"	,
            "開放回應題目二",
            "買回菸品價錢",
            "買回菸品條碼",
            "未買回菸品原因",
            "發放的禮卷金額",
            "查核成績",
            "審核狀態",
        ];
    }
    
 
    public function map($row) : array
    {
        $products1 = implode(",",$row->products1->map(function ($item,$key) {
            return $item->name ." ".$item->describe;
        })->all());
        
        $products1 = str_replace(  "," , "\r", $products1);
         
            
        $products2 = implode(",",$row->products3->map(function ($item,$key) {
            return $item->name ." ".$item->describe;
        })->all());
        
        $products2 = str_replace(  "," , "\r", $products2);
        
        $products3 = implode(",",$row->products3->map(function ($item,$key) {
            return $item->name ." ".$item->describe;
        })->all());
        
        $products3 = str_replace(  "," , "\r", $products3);
        
        $products4 = implode(",",$row->products4->map(function ($item,$key) {
            return $item->name ." ".$item->describe;
        })->all());
            
        $products4 = str_replace(  "," , "\r", $products3);
        
     
        
        return [
            $row->store->area,
            $row->store->path_partner_no,
            $row->fma,
            $row->store->sales_area_no,
            $row->store->store_no,
            $row->store->name,
            $row->store->area_name ." ". $row->store->addr ,
            $row->store->addr_descript,
            $row->addr_memo,
            $row->access_date,
            Answer::ACCESS_STATUS[$row->access_status],
            $row->access_memo,
            $row->topic_0_answers,
            implode("\r", $row->topic_1_answers) ,
            $products1,
            $row->topic_1_answers_other,
            implode("\r", $row->topic_2_answers),
            $products2,
            $row->topic_2_answers_other,
            implode("\r", $row->topic_3_answers),
            $products3,
            $row->topic_3_answers_other,
            $row->topic_4_answers,
            $products4,
            $row->topic_4_answers_other,
            $row->topic_open_1_answers,
            $row->topic_open_2_answers,
          
            $row->tobacco_price,
            $row->tobacco_code." ",
            $row->tobacco_reason,
            $row->voucher,
            $row->getAnswerScore(),
            Answer:: STATUS[$row->status],
        ];
    } 
    
    public function columnWidths(): array
    {
        return [
            'A' => 15,
            'B' => 10,
            'C' => 15,
            'D' => 15,
            'E' => 15,
            'F' => 15,
            'G' => 30,
            'H' => 25,
            'I' => 15,
            'J' => 20,
            'K' => 20,
           
            'L' => 15,
            'M' => 40,
            'N' => 40,
            'O' => 40,
            'P' => 40,
            'Q' => 40,
            "R"=> 40,
            "S"=> 40,
            "T"=> 40,
            "U"=> 40,
            "V"=> 40,
            "W"=> 40,
            "X"=> 40,
            "Y"=> 40,
            "Z"=> 40,
            'AA' => 20,
            'AB' => 20,
            'AC' => 20,
            'AD' => 20,
            'AE' => 15,
            'AF' => 10,
            'AG' => 10,
             
        ];
    }
    
}


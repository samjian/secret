<?php

namespace App\Admin\Actions;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;

class ActionButton extends RowAction
{
    public $name;
    public $href;
    
    public function __construct($name, $href){
      
        $this->name = $name;
        $this->href =$href;
    }
    
    public function href()
    {
       
        return $this->href;
    }
    
    
    public function handle(Model $model)
    {
       
    }
}
<?php
namespace App\Admin\Actions;

use Encore\Admin\Actions\Action;
use Illuminate\Http\Request;

class ToolExport extends Action
{
    protected $selector = '.import-post';
    
    public  $name = "";
    
    public $url ="";
    
    public $msg="";
    
    public function __construct($name, $url ,$msg){
        $this->name = $name;
        $this->url =$url;
        $this->msg =$msg;
    }
    
    public function handle(Request $request)
    {
        return $this->response()->success($this->msg);
    }
    
    public function html()
    {
        return <<<HTML
        <a href="$this->url" target="_blank"  class="btn btn-sm btn-success">$this->name</a>
        HTML;
    }
}
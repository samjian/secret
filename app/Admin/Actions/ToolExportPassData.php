<?php
namespace App\Admin\Actions;

use Encore\Admin\Actions\Action;
use Illuminate\Http\Request;
use App\Models\Campaign;

class ToolExportPassData extends Action
{
    protected $selector = '.import-post';
    public  $name = "匯出審核通過資料";
     
    public function __construct($name){
        $this->name = $name;
    }
    
    
    public function handle(Request $request)
    {
        // $request ...
        
        return $this->response()->success('匯出成功');
    }
    
    public function html()
    {
        
        $campaign_id = request()->cid ? request()->cid:request()->c;
        
        $area = request()->area_tyep ? Campaign ::AREA[ request()->area_tyep] :request()->area  ;
         
        $url = url("/export/answerPass?cid=$campaign_id&area=$area");
        
        return <<<HTML
        <a href="$url" target="_blank"  class="btn btn-sm btn-success">$this->name</a>
        HTML;
        
    }
}
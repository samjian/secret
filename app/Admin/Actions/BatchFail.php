<?php

namespace App\Admin\Actions;

use Encore\Admin\Actions\BatchAction;
use Illuminate\Database\Eloquent\Collection;

class BatchFail extends BatchAction
{
    public $name = '批次不通過';

    public function handle(Collection $collection)
    {
        foreach ($collection as $model) {
            $model->status = 2;
            $model->save();
        }
  
        return $this->response()->success('Success message...')->refresh();
    }
}
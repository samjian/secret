<?php
namespace App\Admin\Actions;

use Encore\Admin\Actions\Action;
use Illuminate\Http\Request;
use App\Models\Campaign;
 
class ToolAnalyzeExportData extends Action
{
    protected $selector = '.import-post';
    public  $name = "匯出審核通過資料";
    public  $file_name = "";
    
    public function __construct($name, $file_name){
        $this->name = $name;
        $this->file_name = $file_name ;
    }
    
    
    public function handle(Request $request)
    {
        // $request ...
        
        return $this->response()->success('匯出成功');
    }
    
    public function html()
    {
        $str  = request()->getQueryString();
        
        $file_name = $this->file_name;
        
        $url = url("/export/analyzeExportData?file_name=$file_name&$str");
        
        return <<<HTML
        <a href="$url" target="_blank"  class="btn btn-sm btn-success">$this->name</a>
        HTML;
        
    }
}
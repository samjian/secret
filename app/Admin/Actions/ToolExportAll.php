<?php
namespace App\Admin\Actions;

use Encore\Admin\Actions\Action;
use Illuminate\Http\Request;

class ToolExportAll extends Action
{
    protected $selector = '.import-post';
    
 
    
    public function handle(Request $request)
    {
        // $request ...
        
        return $this->response()->success('匯出成功');
    }
    
    public function html()
    {
        $campaign_id = request()->c;
     
        $url = url("/export/answerAll/$campaign_id");
        return <<<HTML
        <a href="$url" target="_blank"  class="btn btn-sm btn-warning">匯出所有資料</a>
        HTML;
    }
}
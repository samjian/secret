<?php

namespace App\Admin\Controllers;


use App\Http\Controllers\Controller;
use App\Models\Answer;
use App\Models\Campaign;
use App\Models\Product;
use App\Models\Store;
use Encore\Admin\Admin;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

 
use App\Admin\Actions\ActionButton;
use App\Admin\Actions\BatchFail;
use App\Admin\Actions\BatchPass;
use App\Admin\Actions\ToolExportAll;
use App\Admin\Actions\ToolExportPassData;
use App\Utils\Imgcompress;
 
 
 

class AnswerController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header("答卷")
            ->description("管理")
            ->body($this->grid());
    }
 
    
    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header("答卷")
            ->description("檢視")
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header("答卷")
            ->description("編輯")
            ->body($this->form()->edit($id));
    }

    public function approved($id) {
        $answer = Answer::find($id);
       
        $c = request()->c;
        
        if ($answer ->status !=0) {
            admin_toastr('答卷已送過審核', 'error');
            return redirect("admin/answer?c=$c");
        }
     
        $answer ->status =1;
        $answer->save();
         
        admin_toastr('送審核成功', 'success');
        return redirect("admin/answer?c=$c");
    }
    
    public function pass($id) {
        $c = request()->c;
        $answer = Answer::find($id);
                   
        $answer ->status =2; 
        $answer->review_user_id = auth()->user()->id;
        $answer->save();
               
        
        admin_toastr('審核成功', 'success');
        return redirect("admin/answer?c=$c");
    }
    
   
    
    public function nopass($id) {
        $c = request()->c;
     
        $answer = Answer::find($id);
        $answer ->status =3;
        $answer->review_user_id = auth()->user()->id;
        $answer->save();
        
       
        admin_toastr('審核不通過成功', 'success');
        return redirect("admin/answer?c=$c");
    }
 
 
    
    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header("答卷")
            ->description("建立")
            ->body($this->form());
            //->row("「＊請點選上方「答卷」頁籤以繼續完成本問卷」");
    }

    
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        
        $c_id = request()->c;
       
        if (!$c_id)  {
            return ;
        }
       
        $campaign = Campaign::find($c_id);
        
        $user = auth()->user();
        
        $isManager= $user->roles()->whereIn("slug",["manager", "approver"])->exists() || $user->isAdministrator();
        
        $grid = new Grid(new Answer);
        $grid->disableExport();
        $grid->model()->where("campaign_id" , $campaign ->id);
      
        
        if ($isManager) {
            $grid->model()->where("status","<>" ,0 );
        
            $grid->model()->orderByRaw('field(status ,1,3,2)', 'asc');
            
            
            
            //field(product_image_label , 0,17,16,15,14,13,12,11,10,9,8,7,6,5,4,2,3,1) desc
            $grid->header(function ($query) use ($campaign) {
                return "<b>目前審核的活動是  &nbsp;&nbsp;:&nbsp;$campaign->no&nbsp;&nbsp;$campaign->name</b>";
            });
            
            //加入批次通過
            $grid->batchActions(function ($batch) {
                $batch->add(new  BatchPass());
                $batch->add(new  BatchFail());
            });
                
            //加入匯出功能按鈕
            $grid->tools(function (Grid\Tools $tools) {
                $tools->append(new ToolExportAll());
                $tools->append(new ToolExportPassData("匯出審核通過資料"));
            });
            
            $grid->disableCreateButton();
        } else {
            //秘密客只能看到自己新增的答卷
            $grid->model()->where("admin_user_id" , $user->id);
            $grid->model()->orderByRaw('field(status , 0 , 3,1,2)', 'asc');
        }
   
            
        $grid->filter(function($filter) use ($isManager){
            $filter->disableIdFilter();
            $filter->contains('store.store_no',"客戶店號");
            $filter->contains('store.store_code',"請輸入編碼搜尋店家");
            
            $filter->contains('store.name',"店家名稱");
            if ($isManager) {
                $filter->equal('status',"狀態")->radio([             
                    1    => '送審',
                    2    => '審核通過 ',
                    3 =>"不通過"
                ]);
            } else {
                $filter->equal('status',"狀態")->radio([
                    0    => '暫存',
                    1    => '送審',
                    2    => '審核通過 ',
                    3 =>"不通過"
                ]);
                
            }
            
           
            $filter->between("created_at","建立日期")->date();
            
        });
           
        
        $grid->actions(function ($actions) use($campaign,$isManager ){
            $actions->disableEdit();
            $actions->disableView();
            
            $id = $this->row->id;
            
            $actions->add(new ActionButton("檢視", url("/admin/answer/$id?c=$campaign->id")));
            
            if ($isManager) {
                $actions->add(new ActionButton("編輯", url("/admin/answer/$id/edit?c=$campaign->id")));
                $actions->add(new ActionButton("通過", url("/admin/answer/$id/pass?c=$campaign->id")));
                $actions->add(new ActionButton("不通過", url("/admin/answer/$id/nopass?c=$campaign->id")));
               
            } else {
                $actions->add(new ActionButton("編輯", url("/admin/answer/$id/edit?c=$campaign->id")));
                $actions->add(new ActionButton("送審", url("/admin/answer/$id/approved?c=$campaign->id")));
            }
        
        });
        
        $grid->column('store.store_no' , "客戶店號" );
        $grid->column('store.store_code' , "店家編碼" );
   
        $grid->column('store.name' , "店家名稱");
        $grid->column('store.area_name' , "縣市");
        
        $grid->column('user.name' , "填單人");
        
        $grid->column("campaign.voucher_type",'禮券類別')->using(Campaign::VOUCHAR_ALL_TYPE);
        $grid->voucher('發放禮卷金額');
        $grid->score('得分');
      
        
        $grid->column("status",'狀態')->using([
            0 => '暫存',
            1 => '送審',
            2 => '審核通過',
            3 => '不通過',
        ])->label([
            0 => 'default',
            1 => 'warning',
            2 => 'success',
            3 => 'danger',
        ]);
        
        $grid->column("img_path",'進入檢視')->display(function($img_path)  {
            if (!$img_path) return "尚無檔案";
            
            $url = config("filesystems.disks.admin.url");
            $arr = explode(".", $img_path);
            $path = $arr[0].".".$arr[1];
            return  "
                     <a href='$url/$img_path' target='_blank'>
                        <img src='$url/$path' style='max-width:200px;max-height:200px' class='img img-thumbnail'>
                   </a>
                   ";
        });
        
        $grid->column("recording_path",'錄音檔')->display(function($recording_path)  {
            if (!$recording_path) return "尚無檔案";

            $url = config("filesystems.disks.admin.url");
            
            return  "
                     <a href='$url/$recording_path' target='_blank'>
                                                                                     播放
                       </a>
                   ";
        });
         
        //$grid->column('reviewUser.name',"審核者");
        
        
        $grid->created_at("建立日期");
        $grid->updated_at("最後儲存日期");
        
       
        return $grid;
    }

 

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $anser =  Answer::findOrFail($id);
        $campaign = $anser->campaign()->first();
        
        $user = auth()->user();
        
        $isManager= $user->roles()->whereIn("slug",["manager", "approver"])->exists() || $user->isAdministrator();
        
        $show = new Show($anser);
        $show->panel()->tools(function ($tools) use($anser , $campaign , $isManager) {
            $tools->disableEdit();
            //$tools->disableList();
            $tools->disableDelete();
           
            if($isManager) {
                $url_pass = url("/admin/answer/$anser->id/pass?c=$campaign->id");
                $url_no_pass =  url("/admin/answer/$anser->id/nopass?c=$campaign->id");
                
                $tools->append('<a id="sendReview" href="'.$url_pass.'" class="btn btn-sm btn-success"><i class="fa fa-arrow-up"></i>通過</a>');
                $tools->append('<a id="sendReview" href="'.$url_no_pass.'" class="btn btn-sm btn-danger"><i class="fa fa-arrow-down"></i>不通過</a>');
            }
        });
            
            
            
        $show->field("campaign.no","活動編號");
        $show->field("campaign.name","活動名稱");
        $show->field("store.name","受訪店家");
        $show->field("store.addr","店家住址");
        $show->field("store.addr_descript","店家備註");
        $show->field("addr_memo","地址補充");
        
        
        $show->access_date('店訪日期');
        $show->access_status('店訪狀態')->using(Answer::ACCESS_STATUS);
     
        $show->access_memo("電訪說明");
        
        $show->divider();
        
        
        $show->topic_0("直接淘汰題")->as(function () use($campaign){
            return $campaign->topic_0;
        });
            
        $show->topic_0_answers('淘汰原因');
        
        $show->topic_1("題目一")->as(function () use($campaign){
            return $campaign->topic_1;
        });
        
       
        $show->topic_1_answers('答覆')->unescape()->as(function ($texts) {
            return implode("<br>", $texts);
        });
        
        $show->products1('店家回應其他菸品')->unescape()->as(function ($rows)  {
            $html ="";
            foreach ($rows as $row ) {
                $html.= "<div>".$row->name ." " .$row->describe."</div>";
            }
            
            return $html;
        });
        $show->topic_1_answers_other('其他回應');
        
        $show->divider();
        
        $show->topic_2('題目二')->as(function () use($campaign){
            return $campaign->topic_2;
        });
            
       
        
        $show->topic_2_answers('答覆')->unescape()->as(function ($texts) {
            return implode("<br>", $texts);
        });
        
        $show->products2('店家回應其他菸品')->unescape()->as(function ($rows)  {
            $html ="";
            foreach ($rows as $row ) {
                $html.=  "<div>".$row->name ." " .$row->describe."</div>";
            }
            
            return $html;
        });
        $show->topic_2_answers_other('其他回應');
            
        $show->divider();
            
        $show->topic_3('題目三')->as(function () use($campaign){
            return $campaign->topic_3;
        });
      
        $show->topic_3_answers('答覆')->unescape()->as(function ($texts) {
            return implode("<br>", $texts);
        });
                
       
        $show->products3('店家回應其他菸品')->unescape()->as(function ($rows)  {
            $html ="";
            foreach ($rows as $row ) {
                $html.=  "<div>".$row->name ." " .$row->describe."</div>";
            }
         
            return $html;
        });
        
        $show->topic_3_answers_other('其他回應');
        
        $show->divider();
        
        $show->topic_4('題目四 (不列入評分)')->as(function () use($campaign){
            return $campaign->topic_3;
        });
        $show->topic_4_answers('答覆');
            
        $show->products4('店家回應其他菸品')->unescape()->as(function ($rows)  {
            $html ="";
            foreach ($rows as $row ) {
                $html.=  "<div>".$row->name ." " .$row->describe."</div>";
            }
            
            return $html;
        });
                
        $show->topic_4_answers_other('其他回應');
        
        $show->divider();
        $show->topic_open_1_answers('開放回應題目一');
        $show->topic_open_2_answers('開放回應題目二');
        
        
        $show->divider();
        $show->voucher('發放禮卷金額');
        $show->tobacco_price('買回菸品價錢');
        $show->tobacco_code('買回菸品條碼');
        $show->tobacco_reason('未買回菸品原因');
        $show->img_path('上傳照片檔')->image();
        $show->recording_path('上傳錄音檔')->file();;
        
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));
        
        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {             
        $c_id = request()->c;
        
        if ($c_id == null ) return new Form(new Answer);
        
        $campaign = Campaign::find($c_id);
               
        $product_arr = [];
        
       

        foreach (Product::all() as $product) {
            $product_arr[$product->id] = $product->name ." " .$product->describe;
        }
     
        
        $strore_arr = [
         
        ];
        
        $stores =[];
        
        if ($campaign->area_tyep  == 0) {
            $stores =  Store::all();
        } else {
            $stores =  Store::where("area", Campaign::AREA[$campaign->area_tyep])->get();
        }
     
        foreach ($stores  as $store) {
            $strore_arr[$store->store_code] = $store->store_no ."   ". $store->name;
        }
        
       
        $form = new Form(new Answer);
        $form->disableViewCheck();
        /*
        if ($form->isCreating()) {
            $form->builder()->getFooter()->checkCreating();
        } else {
            $form->builder()->getFooter()->checkEditing();
        }*/
   
        
        $form->tools(function (Form\Tools $tools) use($campaign) {
            $tools->disableView();
            $tools->disableDelete();
             
            //$url =  url("/admin/answer/$id/approved?c=$campaign->id");
            
            $tools->add('<a id="sendReview" href="javascript:void(0)" class="btn btn-sm btn-success"><i class="fa fa-arrow-up"></i>送審</a>');
        });
        
        $form->footer(function ($footer) {
             
            // 去掉`查看`checkbox
            $footer->disableViewCheck();
            
           
            
        });
            
        $form->hidden('c')->value($campaign->id);
        $form->hidden('campaign_id')->value($campaign->id);
        $form->hidden('admin_user_id')->value(auth()->user()->id);
        $form->hidden('score')->value(0);
        $form->hidden('topic_1_answers_score')->value(0);
        $form->hidden('topic_2_answers_score')->value(0);
        $form->hidden('topic_3_answers_score')->value(0);
        
        $form->hidden('status')->value(0);
        $form->ignore(['c'] );
        
        
        
        $form->tab('活動及店家基本資訊', function ($form)  use ($campaign, $strore_arr){
            
            $form->display("活動編號")->with(function ($value) use ($campaign) {
                return $campaign->no;
            });
                
            $form->display("活動名稱")->with(function ($value) use ($campaign) {
                return $campaign->name;
            });
               
                    
            $form->select('store_code', '受訪店家編碼' )->options(function() use ($strore_arr){
               return $strore_arr;
            })->rules('required',[
                "required" =>"請選擇受訪店家編碼"
            ]);
            
         
            
            $form->display("test", "店家住址")->with(function ($value) {
                return "<div id='storeAddr'></div>";
            });
            
            $form->display("店家備註")->with(function ($value) {
                return "<div id='storeAddrDescript'></div>";
            });
                              
            $form->text('addr_memo', "地址補充");
            $form->datetime('access_date', '店訪日期')->value(date("Y-m-d H:i:s"))->required();
            
            $form->select('access_status', '店訪狀態') ->required()->options( Answer::ACCESS_STATUS);
            
            
            $form->textarea('access_memo', '店訪狀態說明')->rows(3);
            $form->html("<font color='red'>「＊請點選上方「答卷」頁籤以繼續完成本問卷」</font>");
           
            
        })->tab('答卷', function ($form) use ($campaign , $product_arr) {
            if ($campaign->topic_0) {
                $form->display("直接淘汰題")->with(function ($value) use ($campaign) {
                    return "<fon  style='color:#00F;'><b>$campaign->topic_0</b></font>";
                });
                
                
               
                $form->select("topic_0_answers" , "淘汰原因")->options(function() use ($campaign){
                    $arr=[];
                    
                    if ($campaign->topic_0_a)$arr [$campaign->topic_0_a] = $campaign->topic_0_a;
                    if ($campaign->topic_0_b)$arr [$campaign->topic_0_b] = $campaign->topic_0_b;
                    if ($campaign->topic_0_c)$arr [$campaign->topic_0_c] = $campaign->topic_0_c;
                    if ($campaign->topic_0_d)$arr [$campaign->topic_0_d] = $campaign->topic_0_d;
                    if ($campaign->topic_0_e)$arr [$campaign->topic_0_e] = $campaign->topic_0_e;
                    if ($campaign->topic_0_f)$arr [$campaign->topic_0_f] = $campaign->topic_0_f;
                    if ($campaign->topic_0_g)$arr [$campaign->topic_0_g] = $campaign->topic_0_g;
                    if ($campaign->topic_0_h)$arr [$campaign->topic_0_h] = $campaign->topic_0_h;
                    if ($campaign->topic_0_i)$arr [$campaign->topic_0_i] = $campaign->topic_0_i;
                    if ($campaign->topic_0_j)$arr [$campaign->topic_0_j] = $campaign->topic_0_j;
                    
                    return $arr;
                });
            
            
               
            }  
            if($campaign->topic_1) {
                $form->display("題目一")->with(function ($value) use ($campaign) {
                    return "<fon  style='color:#00F;'><b>$campaign->topic_1</b></font>";
                });
                   
                if ($campaign->topic_1_type) {
                   
                    $form->multipleSelect("topic_1_answers" , "答覆")->options(function() use ($campaign){
                        $arr=[];
                        
                        if ($campaign->topic_1_a)$arr [$campaign->topic_1_a] = $campaign->topic_1_a;
                        if ($campaign->topic_1_b)$arr [$campaign->topic_1_b] = $campaign->topic_1_b;
                        if ($campaign->topic_1_c)$arr [$campaign->topic_1_c] = $campaign->topic_1_c;
                        if ($campaign->topic_1_d)$arr [$campaign->topic_1_d] = $campaign->topic_1_d;
                        if ($campaign->topic_1_e)$arr [$campaign->topic_1_e] = $campaign->topic_1_e;
                        if ($campaign->topic_1_f)$arr [$campaign->topic_1_f] = $campaign->topic_1_f;
                        if ($campaign->topic_1_g)$arr [$campaign->topic_1_g] = $campaign->topic_1_g;
                        if ($campaign->topic_1_h)$arr [$campaign->topic_1_h] = $campaign->topic_1_h;
                        if ($campaign->topic_1_i)$arr [$campaign->topic_1_i] = $campaign->topic_1_i;
                        if ($campaign->topic_1_j)$arr [$campaign->topic_1_j] = $campaign->topic_1_j;
                        
                        return $arr;
                    });
                } else {
                    $form->select("topic_1_answers" , "答覆")->options(function() use ($campaign){
                        $arr=[];
                        
                        if ($campaign->topic_1_a)$arr [$campaign->topic_1_a] = $campaign->topic_1_a;
                        if ($campaign->topic_1_b)$arr [$campaign->topic_1_b] = $campaign->topic_1_b;
                        if ($campaign->topic_1_c)$arr [$campaign->topic_1_c] = $campaign->topic_1_c;
                        if ($campaign->topic_1_d)$arr [$campaign->topic_1_d] = $campaign->topic_1_d;
                        if ($campaign->topic_1_e)$arr [$campaign->topic_1_e] = $campaign->topic_1_e;
                        if ($campaign->topic_1_f)$arr [$campaign->topic_1_f] = $campaign->topic_1_f;
                        if ($campaign->topic_1_g)$arr [$campaign->topic_1_g] = $campaign->topic_1_g;
                        if ($campaign->topic_1_h)$arr [$campaign->topic_1_h] = $campaign->topic_1_h;
                        if ($campaign->topic_1_i)$arr [$campaign->topic_1_i] = $campaign->topic_1_i;
                        if ($campaign->topic_1_j)$arr [$campaign->topic_1_j] = $campaign->topic_1_j;
                        return $arr;
                    });
                }
            
            
               
                                
                                
                if ($campaign->topic_1_answer) {
                    $form->listbox("products1","店家回應其他菸品")->options($product_arr);
                }
                                
                $form->textarea('topic_1_answers_other', '其他回應');
                
                $form->radioButton('topic_1_answers_check', '是否正確答覆')->options([1 => '是', 0 => '否'])->default('1');
            }     
              
            
            if($campaign->topic_2) {
                $form->display("題目二")->with(function ($value) use ($campaign) {
                    return "<fon  style='color:#00F;'><b>$campaign->topic_2</b></font>";
                });
                
                if ($campaign->topic_2_type) {
                     
                   
                    $form->multipleSelect("topic_2_answers" , "答覆")->options(function() use ($campaign){
                        $arr=[];
                        
                        if ($campaign->topic_2_a)$arr [$campaign->topic_2_a] = $campaign->topic_2_a;
                        if ($campaign->topic_2_b)$arr [$campaign->topic_2_b] = $campaign->topic_2_b;
                        if ($campaign->topic_2_c)$arr [$campaign->topic_2_c] = $campaign->topic_2_c;
                        if ($campaign->topic_2_d)$arr [$campaign->topic_2_d] = $campaign->topic_2_d;
                        if ($campaign->topic_2_e)$arr [$campaign->topic_2_e] = $campaign->topic_2_e;
                        if ($campaign->topic_2_f)$arr [$campaign->topic_2_f] = $campaign->topic_2_f;
                        if ($campaign->topic_2_g)$arr [$campaign->topic_2_g] = $campaign->topic_2_g;
                        if ($campaign->topic_2_h)$arr [$campaign->topic_2_h] = $campaign->topic_2_h;
                        if ($campaign->topic_2_i)$arr [$campaign->topic_2_i] = $campaign->topic_2_i;
                        if ($campaign->topic_2_j)$arr [$campaign->topic_2_j] = $campaign->topic_2_j;
                        
                        return $arr;
                    });
                        
                } else {
                    $form->select("topic_2_answers" , "答覆")->options(function() use ($campaign){
                        $arr=[];
                        
                        if ($campaign->topic_2_a)$arr [$campaign->topic_2_a] = $campaign->topic_2_a;
                        if ($campaign->topic_2_b)$arr [$campaign->topic_2_b] = $campaign->topic_2_b;
                        if ($campaign->topic_2_c)$arr [$campaign->topic_2_c] = $campaign->topic_2_c;
                        if ($campaign->topic_2_d)$arr [$campaign->topic_2_d] = $campaign->topic_2_d;
                        if ($campaign->topic_2_e)$arr [$campaign->topic_2_e] = $campaign->topic_2_e;
                        if ($campaign->topic_2_f)$arr [$campaign->topic_2_f] = $campaign->topic_2_f;
                        if ($campaign->topic_2_g)$arr [$campaign->topic_2_g] = $campaign->topic_2_g;
                        if ($campaign->topic_2_h)$arr [$campaign->topic_2_h] = $campaign->topic_2_h;
                        if ($campaign->topic_2_i)$arr [$campaign->topic_2_i] = $campaign->topic_2_i;
                        if ($campaign->topic_2_j)$arr [$campaign->topic_2_j] = $campaign->topic_2_j;
                        return $arr;
                    });
                }
                       
                                        
                if ($campaign->topic_2_answer) {
                    $form->listbox("products2","店家回應其他菸品")->options($product_arr);
                }
                                        
                $form->textarea('topic_2_answers_other', '其他回應');
                $form->radioButton('topic_2_answers_check', '是否正確答覆')->options([1 => '是', 0 => '否'])->default('1');
            }   
            
            if($campaign->topic_3) {
                $form->display("題目三")->with(function ($value) use ($campaign) {
                    return "<fon  style='color:#00F;'><b>$campaign->topic_3</b></font>";
                });
                
                if ($campaign->topic_3_type) {
                     
                    $form->multipleSelect("topic_3_answers" , "答覆")->options(function() use ($campaign){
                        $arr=[];
                        
                        if ($campaign->topic_3_a)$arr [$campaign->topic_3_a] = $campaign->topic_3_a;
                        if ($campaign->topic_3_b)$arr [$campaign->topic_3_b] = $campaign->topic_3_b;
                        if ($campaign->topic_3_c)$arr [$campaign->topic_3_c] = $campaign->topic_3_c;
                        if ($campaign->topic_3_d)$arr [$campaign->topic_3_d] = $campaign->topic_3_d;
                        if ($campaign->topic_3_e)$arr [$campaign->topic_3_e] = $campaign->topic_3_e;
                        if ($campaign->topic_3_f)$arr [$campaign->topic_3_f] = $campaign->topic_3_f;
                        if ($campaign->topic_3_g)$arr [$campaign->topic_3_g] = $campaign->topic_3_g;
                        if ($campaign->topic_3_h)$arr [$campaign->topic_3_h] = $campaign->topic_3_h;
                        if ($campaign->topic_3_i)$arr [$campaign->topic_3_i] = $campaign->topic_3_i;
                        if ($campaign->topic_3_j)$arr [$campaign->topic_3_j] = $campaign->topic_3_j;
                        return $arr;
                    });
                        
                } else {
                    $form->select("topic_3_answers" , "答覆")->options(function() use ($campaign){
                        $arr=[];
                        
                        if ($campaign->topic_3_a)$arr [$campaign->topic_3_a] = $campaign->topic_3_a;
                        if ($campaign->topic_3_b)$arr [$campaign->topic_3_b] = $campaign->topic_3_b;
                        if ($campaign->topic_3_c)$arr [$campaign->topic_3_c] = $campaign->topic_3_c;
                        if ($campaign->topic_3_d)$arr [$campaign->topic_3_d] = $campaign->topic_3_d;
                        if ($campaign->topic_3_e)$arr [$campaign->topic_3_e] = $campaign->topic_3_e;
                        if ($campaign->topic_3_f)$arr [$campaign->topic_3_f] = $campaign->topic_3_f;
                        if ($campaign->topic_3_g)$arr [$campaign->topic_3_g] = $campaign->topic_3_g;
                        if ($campaign->topic_3_h)$arr [$campaign->topic_3_h] = $campaign->topic_3_h;
                        if ($campaign->topic_3_i)$arr [$campaign->topic_3_i] = $campaign->topic_3_i;
                        if ($campaign->topic_3_j)$arr [$campaign->topic_3_j] = $campaign->topic_3_j;
                        return $arr;
                    });
                }
                
                    
                    
                if ($campaign->topic_3_answer) {
                   $form->listbox("products3","店家回應其他菸品")->options($product_arr);
                }
                
                $form->textarea('topic_3_answers_other', '其他回應');
                $form->radioButton('topic_3_answers_check', '是否正確答覆')->options([1 => '是', 0 => '否'])->default('1');
            }
            
            
            if($campaign->topic_4) {
                $form->display("題目四 (不列入評分)")->with(function ($value) use ($campaign) {
                    return "<fon  style='color:#00F;'><b>$campaign->topic_4</b></font>";
                });
                    
                $form->select("topic_4_answers" , "答覆")->options(function() use ($campaign){
                    $arr=[];
                    
                    if ($campaign->topic_4_a)$arr [$campaign->topic_4_a] = $campaign->topic_4_a;
                    if ($campaign->topic_4_b)$arr [$campaign->topic_4_b] = $campaign->topic_4_b;
                    if ($campaign->topic_4_c)$arr [$campaign->topic_4_c] = $campaign->topic_4_c;
                    if ($campaign->topic_4_d)$arr [$campaign->topic_4_d] = $campaign->topic_4_d;
                    if ($campaign->topic_4_e)$arr [$campaign->topic_4_e] = $campaign->topic_4_e;
                    if ($campaign->topic_4_f)$arr [$campaign->topic_4_f] = $campaign->topic_4_f;
                    if ($campaign->topic_4_g)$arr [$campaign->topic_4_g] = $campaign->topic_4_g;
                    if ($campaign->topic_4_h)$arr [$campaign->topic_4_h] = $campaign->topic_4_h;
                    if ($campaign->topic_4_i)$arr [$campaign->topic_4_i] = $campaign->topic_4_i;
                    if ($campaign->topic_4_j)$arr [$campaign->topic_4_j] = $campaign->topic_4_j;
                    return $arr;
                });
                        
                
                if ($campaign->topic_4_answer) {
                    $form->listbox("products4","店家回應其他菸品")->options($product_arr);
                }
                
                $form->textarea('topic_4_answers_other', '其他回應');
            }
            
            if ($campaign->topic_open_1) {
                $form->display("開放回應題目一")->with(function ($value) use ($campaign) {
                    return "<fon  style='color:#00F;'><b>$campaign->topic_open_1</b></font>";
                });
                
                $form->textarea('topic_open_1_answers', '開放回應題目一 回覆');
            }
            
            
            if ($campaign->topic_open_2) {
                $form->display("開放回應題目二")->with(function ($value) use ($campaign) {
                    return "<fon  style='color:#00F;'><b>$campaign->topic_open_2</b></font>";
                });
                $form->textarea('topic_open_2_answers', '開放回應題目二 回覆');
            }
            $form->html("<font color='red'>「＊請點選上方「菸品及禮金」頁籤以繼續完成本問卷」</font>");
        })->tab('菸品及禮金', function ($form) use ($campaign){
            $form->currency('tobacco_price', '買回菸品價錢')->symbol('￥')->digits(0);
            $form->select('tobacco_code', '買回菸品條碼')->options(function() use ($campaign){
                $arr=[];
                
                if ($campaign->tobacco_code_1)$arr [$campaign->tobacco_code_1] = $campaign->tobacco_code_1;
                if ($campaign->tobacco_code_2)$arr [$campaign->tobacco_code_2] = $campaign->tobacco_code_2;
                if ($campaign->tobacco_code_3)$arr [$campaign->tobacco_code_3] = $campaign->tobacco_code_3;
                if ($campaign->tobacco_code_4)$arr [$campaign->tobacco_code_4] = $campaign->tobacco_code_4;
                if ($campaign->tobacco_code_5)$arr [$campaign->tobacco_code_5] = $campaign->tobacco_code_5;
                if ($campaign->tobacco_code_6)$arr [$campaign->tobacco_code_6] = $campaign->tobacco_code_6;
                
                
                return $arr;
            });
                
            $form->textarea('tobacco_reason', '未買回菸品原因');
            $form->file('recording_path', '上傳錄音檔')
                 ->uniqueName()
                 ->rules('mimes:mp3,mp4' ,[
                'mimes' => '檔案格式錯誤需為  mp3,mp4',
            ]);
            
            /* 檔案更名
             * $form->image('picture')->name(function ($file) {
                        return 'test.'.$file->guessExtension();
                    });
             */
            
            $form->image('img_path', '上傳照片檔')
            ->uniqueName()//->thumbnail('small', $width = 200, $height = 200)
            ->rules('mimes:jpg,jpeg,png,svg' ,[
                'mimes' => '檔案格式錯誤需為 jpg,jpeg,png,svg',
            ]);
           
            
            if ($campaign->is_voucher) {
                $form->currency('voucher', '發放禮卷金額')->symbol('￥')->digits(0);
            } else {
                $form->hidden('voucher')->value(0);
            }
            
            $form->html("<font color='red' >「＊完成後請於「答卷管理」清單最右方「操作」處點選「送審」」</font>");
        });
          
            
        $form->saving(function (Form $form) use($campaign) {
            $score = 0 ;
            $score2 = 0 ;
            $score3 = 0 ;
            
            if ($form->topic_1_answers_check) {
                if ($campaign->topic_1_type ) {
                    
                    foreach (request()->topic_1_answers as $answer) {
                        
                        if (!$answer) continue;
                        
                        /*
                        if ($campaign->topic_1_a == $answer ) $score += $campaign->topic_1_a_score;
                        else if ($campaign->topic_1_b == $answer ) $score += $campaign->topic_1_b_score;
                        else if ($campaign->topic_1_c == $answer ) $score += $campaign->topic_1_c_score;
                        else if ($campaign->topic_1_d == $answer ) $score += $campaign->topic_1_d_score;
                        else if ($campaign->topic_1_e == $answer ) $score += $campaign->topic_1_e_score;
                        else if ($campaign->topic_1_f == $answer ) $score += $campaign->topic_1_f_score;
                        else if ($campaign->topic_1_g == $answer ) $score += $campaign->topic_1_g_score;
                        else if ($campaign->topic_1_h == $answer ) $score += $campaign->topic_1_h_score;
                        else if ($campaign->topic_1_i == $answer ) $score += $campaign->topic_1_i_score;
                        else if ($campaign->topic_1_j == $answer ) $score += $campaign->topic_1_j_score;*/
                        
                        $score += $campaign->topic_1_a_score;
                        $score += $campaign->topic_1_b_score;
                        $score += $campaign->topic_1_c_score;
                        $score += $campaign->topic_1_d_score;
                        $score += $campaign->topic_1_e_score;
                        $score += $campaign->topic_1_f_score;
                        $score += $campaign->topic_1_g_score;
                        $score += $campaign->topic_1_h_score;
                        $score += $campaign->topic_1_i_score;
                        $score += $campaign->topic_1_j_score;
                        
                        break;
                        
                    }
                } else {
                    if ($campaign->topic_1_a == $form->topic_1_answers ) $score = $campaign->topic_1_a_score;
                    else if ($campaign->topic_1_b == $form->topic_1_answers ) $score = $campaign->topic_1_b_score;
                    else if ($campaign->topic_1_c == $form->topic_1_answers ) $score = $campaign->topic_1_c_score;
                    else if ($campaign->topic_1_d == $form->topic_1_answers ) $score = $campaign->topic_1_d_score;
                    else if ($campaign->topic_1_e == $form->topic_1_answers ) $score = $campaign->topic_1_e_score;
                    else if ($campaign->topic_1_f == $form->topic_1_answers ) $score = $campaign->topic_1_f_score;
                    else if ($campaign->topic_1_g == $form->topic_1_answers ) $score = $campaign->topic_1_g_score;
                    else if ($campaign->topic_1_h == $form->topic_1_answers ) $score = $campaign->topic_1_h_score;
                    else if ($campaign->topic_1_i == $form->topic_1_answers ) $score = $campaign->topic_1_i_score;
                    else if ($campaign->topic_1_j == $form->topic_1_answers ) $score = $campaign->topic_1_j_score;
                }
            }
       
            if ($form->topic_2_answers_check) {
                if ($campaign->topic_2_type) {
                 
                    foreach (request()->topic_2_answers as $answer) {
                        
                        if (!$answer) continue;
                        /*
                        if ($campaign->topic_2_a == $answer ) $score2 += $campaign->topic_2_a_score;
                        else if ($campaign->topic_2_b == $answer ) $score2 += $campaign->topic_2_b_score;
                        else if ($campaign->topic_2_c == $answer ) $score2 += $campaign->topic_2_c_score;
                        else if ($campaign->topic_2_d == $answer ) $score2 += $campaign->topic_2_d_score;
                        else if ($campaign->topic_2_e == $answer ) $score2 += $campaign->topic_2_e_score;
                        else if ($campaign->topic_2_f == $answer ) $score2 += $campaign->topic_2_f_score;
                        else if ($campaign->topic_2_g == $answer ) $score2 += $campaign->topic_2_g_score;
                        else if ($campaign->topic_2_h == $answer ) $score2 += $campaign->topic_2_h_score;
                        else if ($campaign->topic_2_i == $answer ) $score2 += $campaign->topic_2_i_score;
                        else if ($campaign->topic_2_j == $answer ) $score2 += $campaign->topic_2_j_score;
                       */
                        $score2 += $campaign->topic_2_a_score;
                        $score2 += $campaign->topic_2_b_score;
                        $score2 += $campaign->topic_2_c_score;
                        $score2 += $campaign->topic_2_d_score;
                        $score2 += $campaign->topic_2_e_score;
                        $score2 += $campaign->topic_2_f_score;
                        $score2 += $campaign->topic_2_g_score;
                        $score2 += $campaign->topic_2_h_score;
                        $score2 += $campaign->topic_2_i_score;
                        $score2 += $campaign->topic_2_j_score;
                        
                        break;
                    }
               
                } else {
                    if ($campaign->topic_2_a == $form->topic_2_answers ) $score2 = $campaign->topic_2_a_score;
                    else if ($campaign->topic_2_b == $form->topic_2_answers ) $score2 = $campaign->topic_2_b_score;
                    else if ($campaign->topic_2_c == $form->topic_2_answers ) $score2 = $campaign->topic_2_c_score;
                    else if ($campaign->topic_2_d == $form->topic_2_answers ) $score2 = $campaign->topic_2_d_score;
                    else if ($campaign->topic_2_e == $form->topic_2_answers ) $score2 = $campaign->topic_2_e_score;
                    else if ($campaign->topic_2_f == $form->topic_2_answers ) $score2 = $campaign->topic_2_f_score;
                    else if ($campaign->topic_2_g == $form->topic_2_answers ) $score2 = $campaign->topic_2_g_score;
                    else if ($campaign->topic_2_h == $form->topic_2_answers ) $score2 = $campaign->topic_2_h_score;
                    else if ($campaign->topic_2_i == $form->topic_2_answers ) $score2 = $campaign->topic_2_i_score;
                    else if ($campaign->topic_2_j == $form->topic_2_answers ) $score2 = $campaign->topic_2_j_score;
                }
            }
            
            if ($form->topic_3_answers_check) {
                if ($campaign->topic_3_type) {
                    foreach (request()->topic_3_answers as $answer) {
                       
                        if (!$answer) continue;
                        /*
                        if ($campaign->topic_3_a == $answer ) $score3 += $campaign->topic_3_a_score;
                        else if ($campaign->topic_3_b == $answer ) $score3 += $campaign->topic_3_b_score;
                        else if ($campaign->topic_3_c == $answer ) $score3 += $campaign->topic_3_c_score;
                        else if ($campaign->topic_3_d == $answer ) $score3 += $campaign->topic_3_d_score;
                        else if ($campaign->topic_3_e == $answer ) $score3 += $campaign->topic_3_e_score;
                        else if ($campaign->topic_3_f == $answer ) $score3 += $campaign->topic_3_f_score;
                        else if ($campaign->topic_3_g == $answer ) $score3 += $campaign->topic_3_g_score;
                        else if ($campaign->topic_3_h == $answer ) $score3 += $campaign->topic_3_h_score;
                        else if ($campaign->topic_3_i == $answer ) $score3 += $campaign->topic_3_i_score;
                        else if ($campaign->topic_3_j == $answer ) $score3 += $campaign->topic_3_j_score;
                        */
                        $score3 += $campaign->topic_3_a_score;
                        $score3 += $campaign->topic_3_b_score;
                        $score3 += $campaign->topic_3_c_score;
                        $score3 += $campaign->topic_3_d_score;
                        $score3 += $campaign->topic_3_e_score;
                        $score3 += $campaign->topic_3_f_score;
                        $score3 += $campaign->topic_3_g_score;
                        $score3 += $campaign->topic_3_h_score;
                        $score3 += $campaign->topic_3_i_score;
                        $score3 += $campaign->topic_3_j_score;
                        
                        break;
                         
                    }
                } else {
                    if ($campaign->topic_3_a == $form->topic_3_answers ) $score3 = $campaign->topic_3_a_score;
                    else if ($campaign->topic_3_b == $form->topic_3_answers ) $score3 = $campaign->topic_3_b_score;
                    else if ($campaign->topic_3_c == $form->topic_3_answers ) $score3 = $campaign->topic_3_c_score;
                    else if ($campaign->topic_3_d == $form->topic_3_answers ) $score3 = $campaign->topic_3_d_score;
                    else if ($campaign->topic_3_e == $form->topic_3_answers ) $score3 = $campaign->topic_3_e_score;
                    else if ($campaign->topic_3_f == $form->topic_3_answers ) $score3 = $campaign->topic_3_f_score;
                    else if ($campaign->topic_3_g == $form->topic_3_answers ) $score3 = $campaign->topic_3_g_score;
                    else if ($campaign->topic_3_h == $form->topic_3_answers ) $score3 = $campaign->topic_3_h_score;
                    else if ($campaign->topic_3_i == $form->topic_3_answers ) $score3 = $campaign->topic_3_i_score;
                    else if ($campaign->topic_3_j == $form->topic_3_answers ) $score3 = $campaign->topic_3_j_score;
                }
            }
            
            $sum  =  $score + $score2 + $score3;
            
            if ($sum > 100) $sum = 100;
            
            $form->score = $sum;
            $form->topic_1_answers_score = $score;
            $form->topic_2_answers_score = $score2;
            $form->topic_3_answers_score = $score3;
           
            
        });
         
        $form->saved(function (Form $form) use($campaign) {
            
            $obj  =  Answer::find($form->model()->id);
            
            if ($obj->img_path) {
                $img_path = config('filesystems.disks.admin.root')."/".$obj->img_path;
                (new Imgcompress($img_path))->compressImg($img_path);
            }
            
            if ($form->isCreating() && $form->status==1 && request('after-save')) {
                $id=$form->model()->id;
                $url = url("admin/answer/$id/edit?c=".$campaign->id);
                admin_toastr("送審成功");
            } else if (request('after-save') == 1) {
                $id=$form->model()->id;
                $url = url("admin/answer/$id/edit?c=".$campaign->id);
                admin_toastr(trans('admin.update_succeeded'));
            } elseif (request('after-save') == 2) {
                $url = url('admin/answer/create?c='.$campaign->id);
                admin_toastr(trans('admin.save_succeeded'));
            }   else {
                $url = url('admin/answer?c='.$campaign->id);
                admin_toastr(trans('admin.save_succeeded'));
            }
            
           
            return redirect($url);
           
        });
        
        $this->js_form($form);
       
         
       
         
        return $form;
    }
    
    
    /**
             確認答卷是否填寫過
     */
    public function checkAnswer() {
      // "store_code" : store_code, "cid" :cid
     
          $result = Answer::where("store_code" ,request()->store_code )
                        ->where("campaign_id", request()->cid)
                        ->where("admin_user_id" , auth()->user()->id ) ->orderBy("created_at" , "desc")
                        ->first();
          if ($result) {
              echo $result->created_at;
          } else {
              echo "";
          }
    }
    
    private function js_form($form){
        $script = 
        <<<EOT
            function getStoreCode(code) {
            
               var url = "/secret/admin/getStore/" + code ;
               $.get(url,function(store_josn){
                    var store = JSON.parse(store_josn);
                  
                    $("#storeAddr").html(store.area_name + " " + store.addr);
                    $("#storeAddrDescript").html(store.addr_descript);
                });
            }
            
            $("select[name='store_code']").change(function(){
               getStoreCode(this.value);
            });
            
            $(function(){
                $("#btnSend").text("儲存");
                
                var  code = $("select[name='store_code']").val();
                if (code) {
                    getStoreCode(code);
                }
 
                $("#sendReview").click(function(){
                   
                    var form = $(this).parents('form');

                    var store_code = $("select[name='store_code']").val();
                    var cid = $("[name='campaign_id']").val();
                    
                    if (!store_code) {
                        form.submit();
                        return;
                    }
                    
                    var url = "/secret/admin/answer/checkAnswer" ;
                    $.get(url, {"store_code" : store_code, "cid" :cid },function(result_time){
                        if (result_time) {
                            swal({
                                title: "您已於"+result_time+"送審過此店家問卷，您確定還要送審此問卷?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "確認",
                                cancelButtonText: "取消",
                            }).then(function (result) {
                                if (result.value) {
                                  $("input[name='status']").val(1);
                                  form.submit();
                                }
                            });
                        } else {
                             $("input[name='status']").val(1);
                             form.submit();
                        }
                   
                    });
                 });
            })
           
            
        EOT;
        
        Admin::script($script);
    }
}

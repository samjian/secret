<?php

namespace App\Admin\Controllers;

use App\Models\Store;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use  App\Models\Summary;
use App\Models\Campaign;
use App\Admin\Actions\ToolExport;
use App\Admin\Export\SummaryExporter;
use Maatwebsite\Excel\Facades\Excel;

class SummaryController extends Controller
{
    use HasResourceActions;

    
  
    
    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
         
        return $content
            ->header("總覽統計表")
            ->body($this->grid());
    }

    
     

     
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $c_id = request()->c;
        
        //$campaign = Campaign::find($c_id);
        
        $grid = new Grid(new Summary);
        $grid->expandFilter();
        
        //加入匯出功能按鈕
        $grid->tools(function (Grid\Tools $tools) {
            $tools->append(new ToolExport("匯出", url("/admin/summary/export")."?".request()->getQueryString(), "匯出成功"));
        });
        
        $grid->disableActions();
        $grid->disableBatchActions();
        $grid->disableCreateButton();
        $grid->disableRowSelector();
        $grid->disableExport();
     
        
        $grid->filter(function($filter)  {
             
            $filter->disableIdFilter();
            $filter->equal('area_tyep', "地區")->radio(Campaign::AREA );
            
            
            $cs =  Campaign::orderby("startdate", "desc");
            
          
            
            //$filter->in('no',"活動編號")->multipleSelect($cs->pluck('no', 'no'))->default(request()->no);
            $filter->in('cid',"活動名稱")->multipleSelect($cs->pluck('name', 'id'))->default(request()->cid);;
            $filter->between("date","店訪日期")->date();
        });
        
        $grid->no( '活動編號');
        $grid->name( '活動名稱');
        $grid->area( '分區');
        
        $grid->path_partner_no("通路夥伴");
        $grid->store_count("有效查核店家數");
        $grid->two_access("未營業");
        $grid->topic_1_score_count("第一層提問成功家數（得分>0店家)");
        
        $grid->column('合格率一')->display(function()  {
            return round($this->topic_1_score_count / $this->store_count * 100,2)."%";
        });
        
        $grid->topic_2_score_count("第二層提問成功家數（得分>0店家）");
        $grid->column('合格率二')->display(function()  {
            return round($this->topic_2_score_count / $this->store_count * 100,2)."%";
        });
        
        $grid->topic_3_score_count("第三層提問成功家數（得分>0店家）");
        $grid->column('合格率三')->display(function()  {
            return round($this->topic_3_score_count / $this->store_count * 100,2)."%";
        });
        
        $grid->pass("合格率");

      
        return $grid;
    }

    
    public function export(){
       
        return Excel::download(new SummaryExporter(), "總覽統計表.xlsx" );
    }
    
     
}

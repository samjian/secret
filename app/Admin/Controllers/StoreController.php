<?php

namespace App\Admin\Controllers;

use App\Models\Store;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class StoreController extends Controller
{
    use HasResourceActions;

    
    public function getStore($store_code){
       
       echo Store::where("store_code",$store_code)->first()->toJson();;
    }
    
    
    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header(trans('admin.index'))
            ->description(trans('admin.description'))
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header(trans('admin.detail'))
            ->description(trans('admin.description'))
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header(trans('admin.edit'))
            ->description(trans('admin.description'))
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header(trans('admin.create'))
            ->description(trans('admin.description'))
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Store);

        $grid->id('ID');
        $grid->customer_no('customer_no');
        $grid->name('name');
        $grid->area_name('area_name');
        $grid->addr('addr');
        $grid->addr_descript('addr_descript');
        $grid->area('area');
        $grid->path_partner_no('path_partner_no');
        $grid->jti_area_no('jti_area_no');
        $grid->sales_area_no('sales_area_no');
        $grid->fma('fma');
        $grid->created_at(trans('admin.created_at'));
        $grid->updated_at(trans('admin.updated_at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Store::findOrFail($id));

        $show->id('ID');
        $show->customer_no('customer_no');
        $show->name('name');
        $show->area_name('area_name');
        $show->addr('addr');
        $show->addr_descript('addr_descript');
        $show->area('area');
        $show->path_partner_no('path_partner_no');
        $show->jti_area_no('jti_area_no');
        $show->sales_area_no('sales_area_no');
        $show->fma('fma');
        $show->created_at(trans('admin.created_at'));
        $show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Store);

        $form->display('ID');
        $form->text('customer_no', 'customer_no');
        $form->text('name', 'name');
        $form->text('area_name', 'area_name');
        $form->text('addr', 'addr');
        $form->text('addr_descript', 'addr_descript');
        $form->text('area', 'area');
        $form->text('path_partner_no', 'path_partner_no');
        $form->text('jti_area_no', 'jti_area_no');
        $form->text('sales_area_no', 'sales_area_no');
        $form->text('fma', 'fma');
        $form->display(trans('admin.created_at'));
        $form->display(trans('admin.updated_at'));

        return $form;
    }
}

<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Widgets\Box;

use App\Models\Answer;
use App\Models\Campaign;
use Encore\Admin\Grid;
use Encore\Admin\Widgets\Form;
use Illuminate\Support\Facades\DB;
use function Encore\Admin\Helpers\Controllers\orderBy;
use App\Admin\Actions\ToolAnalyzeExportData;
use App\Models\Store;


class MonthController extends Controller
{
    public function index(Content $content)
    {
        $datas = $this->getData();
        
        $box = new Box("查詢條件 ",$this->form());
        $box->collapsable();
        $box->solid();
     
        
        $box2 = new Box("圖表資料",  view('admin.month', ["datas" =>$datas]));
        $box2->collapsable();
        $box2->solid();
       
        
        return $content->header('活動月統計資料')->body($box)->body($box2) ->body($this->grid());
    }
    
    public function removeArrNull($arr) {
       if (!$arr) return [];
       
       $n =0;
       foreach ($arr as $val) {
           if ($val ==null) {
               unset($arr[$n]);
               break;
           }
           $n++;
       }
      
        return $arr;
    }
    
    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function grid()
    {
        //Db::enableQueryLog();
        
        $jti_area_no =$this->removeArrNull( request()->jti_area_no) ;
        $path_partner_no= $this->removeArrNull(request()->path_partner_no);
        $sales_area_no=$this->removeArrNull( request()->sales_area_no);
        $store_code= $this->removeArrNull(request()->store_code);
        $campaign_id= request()->campaign_id;
        //$startdate= request()->startdate;
        //$enddate= request()->enddate;
        
         
        //dd($jti_area_no);
        $grid = new Grid(new Answer);
        //加入匯出功能按鈕
        $grid->tools(function (Grid\Tools $tools) {
            $tools->append(new ToolAnalyzeExportData("匯出" ,"跨活動報表"));
        });
        $grid->disableActions();
        $grid->disableCreateButton();
        $grid->disableFilter();
        
        $grid->disableRowSelector();
        $grid->disableColumnSelector();
        $grid->disableExport();
        $model = $grid->model();
        
        $model->select('answers.score as answer_score','answers.*', 'campaigns.*', 'stores.*');
        $model->join('campaigns', 'campaigns.id', '=', 'answers.campaign_id');
        $model->join('stores', 'stores.store_code', '=', 'answers.store_code');
        
        //$model->where("answers.status" , 2);
        $grid->model()->orderByRaw("field(stores.area ,'北一區', '北二區','中區','南區') asc, campaigns.startdate desc , answer_score desc" );
     
        $model->where("answers.status" , 2);
        $model->whereIn("answers.access_status" , [0,1]);
       
        if (sizeof($path_partner_no) >0 ) {
            $model->whereIn("stores.path_partner_no",   $path_partner_no);
        }
        
        if (sizeof($sales_area_no) > 0) {
            $model->whereIn("stores.sales_area_no",   $sales_area_no);
        }
        
        if (sizeof($store_code) >0) {
            $model->whereIn("stores.store_code",   $store_code);
        }
        
        if ($campaign_id) {
            $model->where("campaigns.id",   $campaign_id);
        } else {
            $model->whereRaw("campaigns.id = (SELECT campaign_id from answers where status =2 and  access_status in(0,1)  order by access_date desc LIMIT  1)");
        }
      
        
        if(sizeof($jti_area_no) >0 ){
            $model->whereIn("stores.jti_area_no", $jti_area_no);
        }
        
        
        if(request()->startdate){
            $model->where("answers.access_date", ">=" , request()->startdate);
        }
        
        if(request()->enddate){
            $model->where("answers.access_date", "<=" , request()->enddate);
        }
        
      
          
        $grid->column('campaign.name' , "活動名稱");
        $grid->column('store.area' , "分區");
        $grid->column('store.path_partner_no' , "TP");
        $grid->column('store.fma' , "FMA");
        $grid->column('store.sales_area_no' , "TPSR");
        $grid->column('store.store_no' , "客戶店號");
        $grid->column('store.name' , "店家名稱");
         
        $grid->column('access_status' , "店訪狀態")->using(Answer::ACCESS_STATUS);
        $grid->column('store.area_name' , "縣市");
      
        $grid->column('voucher_type' , "禮卷類別")->display(function($voucher_type)  {
            return  Campaign::VOUCHAR_ALL_TYPE[$voucher_type];
        });
        
        $grid->column('voucher' , "發放禮卷金額");
        
      
        $grid->column('answer_score' , "得分")->display(function($answer_score)  {
            return $this->getAnswerScore();
        });
      
        $grid->column("img_path",'照片')->display(function($img_path)  {
            if (!$img_path) return "尚無檔案";
            
            $url = config("filesystems.disks.admin.url");
            $arr = explode(".", $img_path);
            $path = $arr[0].".".$arr[1];
            return  "
            <a href='$url/$img_path' target='_blank'>
                              開啟
            </a>
            ";
        });
            
        $grid->column("recording_path",'錄音檔')->display(function($recording_path)  {
                if (!$recording_path) return "尚無檔案";
                
                $url = config("filesystems.disks.admin.url");
                
                return  "
                <a href='$url/$recording_path' target='_blank'>
                播放
                </a>
                ";
         });
        
      //  $grid->column('reviewUser.name' , "審核者");
        $grid->column('access_date' , "訪查時間");
         
       // dd(Db::getQueryLog());
        return $grid ;
    }
    
    public function getData() {
        //DB::enableQueryLog();
        $where ="";
        $whereData =[];
        $sql_pass = "";
        $sql_nopass = "";
       
         
        $jti_area_no =$this->removeArrNull( request()->jti_area_no) ;
      
        $path_partner_no= $this->removeArrNull(request()->path_partner_no);
       
        $sales_area_no=$this->removeArrNull( request()->sales_area_no);
        $store_code= $this->removeArrNull(request()->store_code);
      
        $campaign_id=  request()->campaign_id ;
        
         
        if (sizeof($path_partner_no) >0 ) {
            $str = "'".implode("','" , $path_partner_no)."'";
            $where .= " and s.path_partner_no in ($str)";
        }
       
        if (sizeof($sales_area_no) > 0) {
            $str =  "'".implode("','" , $sales_area_no)."'";
            $where .= " and s.sales_area_no in ($str)";
        }
       
        if (sizeof($store_code) >0) {
            $str =  "'".implode("','" , $store_code)."'";
            $where .= " and s.store_code in ($str)";
        }
        
        if(sizeof($jti_area_no) >0 ){
            $str =  "'".implode("','" , $jti_area_no)."'";
            $where .= " and s.jti_area_no in ($str)";
        }
        
        if ($campaign_id) {           
            $where .= " and c.id =$campaign_id";
        } else {
            $where .= " and c.id =( SELECT campaign_id from answers where status =2 and  access_status in(0,1)  order by access_date desc LIMIT 1)";
        }
         
       
        if(request()->startdate){
            $where .= " and a.access_date >='".request()->startdate."'";
          
        }
        
        if(request()->enddate){
            $where .= " and a.access_date <='".request()->enddate."'";
        
        }
        
         
        //合作通路不合格
        $sql_pass = "
            select c.id cid ,c.name, count(1) answre_count , sum(a.score) score , DATE_FORMAT(a.access_date, '%Y/%m') as gdate
            from campaigns c , answers a , stores s
            where c.id = a.campaign_id 
            and   a.store_code = s.store_code
            and   a.score >= c.score
            and a.status =2  and a.access_status in(0,1) 
            $where 
            group by gdate ,c.id, c.name  
	        order by  gdate asc
        ";
        
        //合作通路布不合格
        $sql_nopass = "
            select   c.id cid ,c.name , count(1) answre_count , sum(a.score) score , DATE_FORMAT(a.access_date, '%Y/%m') as gdate
            from campaigns c , answers a , stores s
            where c.id = a.campaign_id 
            and   a.store_code = s.store_code
            and   a.score < c.score and a.access_status in(0,1) 
            and a.status =2 
            $where 
            group by gdate,  c.id, c.name  
	        order by gdate asc
        ";
          
        $pass =  DB::select($sql_pass, $whereData);
      
        
        $nopass =  DB::select($sql_nopass, $whereData);
      
        $result=[];
        
        foreach ($pass as $row) {
           
            $result[$row->gdate] = [
                "name" => $row->name,
                "pass" =>  $row->score ,
                "nopass" => 0,
                "answre_count"=> $row->answre_count,
                "on_answre_count"=> 0,
                "gdate"=> $row->gdate,
               
            ];
        }
        
        
        foreach ($nopass as $row) {
          
            if (isset($result[$row->gdate])) {
                $temp = $result[$row->gdate];
                $temp["nopass"] = $row->score;
                $temp["on_answre_count"] = $row->answre_count;
            }  else {
                $temp = [];
                $temp["name"] = $row->name;
                $temp["pass"] =0;
                $temp["nopass"] = $row->score;
                $temp["answre_count"] =0;
                $temp["on_answre_count"] = $row->answre_count;
                $temp["gdate"] = $row->gdate;
            }
            
            
            $result[$row->gdate] = $temp;
        }
        
        
        foreach ($result as $key=>$row) {
            $row["answre_rate"] =    round($row["answre_count"] / ($row["answre_count"] + $row["on_answre_count"]) *100 , 2 ) ;
            $row["on_answre_rate"] =   100 - $row["answre_rate"];
            
            $row["answre_avg"] =    round( ($row["pass"] + $row["nopass"]) / ($row["answre_count"] + $row["on_answre_count"]) , 2 ) ;
            
            $result[$key] = $row;
        }
     
        return $result;
    }
    
    
    public function form() {
        $stroe = Store::orderby("path_partner_no", "asc");
        
        $form = new Form(request()->all());
        $form->disableReset();
        $form->method("GET");
        $form->action(url('admin/month'));
        
     
        
       
        $form->multipleSelect("path_partner_no", "TP")->options($stroe->pluck('path_partner_no', 'path_partner_no'));
        $form->multipleSelect("sales_area_no", "TPSR")->options($stroe->pluck('sales_area_no', 'sales_area_no'));
        
        $cs = $stroe->get();
        
        $arr =[];
        foreach ($cs as $c) {
            $arr[$c->store_code]= $c->store_no ." ". $c->name;
        }
        
        $form->multipleSelect("store_code", "店家")->options($arr);
      
       
        $form->select("campaign_id", "活動")->options(Campaign::orderby("startdate", "desc")->pluck('name', 'id'));
        $form->dateRange('startdate', 'enddate',"店訪日期");
        return $form;
        
    }
}
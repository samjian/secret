<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Widgets\Box;

use App\Models\Answer;
use App\Models\Campaign;
use Encore\Admin\Grid;
use Encore\Admin\Widgets\Form;
use Illuminate\Support\Facades\DB;
use function Encore\Admin\Helpers\Controllers\orderBy;
use App\Admin\Actions\ToolExportPassData;
use App\Admin\Actions\ToolAnalyzeExportData;
use Intervention\Image\Size;


class ChartController extends Controller
{
    public function index(Content $content)
    {
     
        $datas = $this->getData();
        
        $box = new Box("", $this->form() ,view('admin.chartjs', [
            "datas" =>$datas]
        ));
       
        return $content->header('Dashboard')->body($box);
    }
    
    
    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function detail(Content $content)
    {
        
        Db::enableQueryLog();
        $form = new Form(request()->all());
        $cid=  request()->cid ;
       // $campaign = Campaign::find($cid);
      
        
        $form->disableReset();
        $form->disableSubmit();
        
        $area = request()->area ? request()->area :Campaign ::AREA[ request()->area_tyep];
          
        
        
        
        $form->display("area","地區")->default($area)->readonly();
        //$form->display("no" , "通路")->readonly();
        $form->display("path_partner_no","通路")->readonly();     
        
        if ( $cid ) {
            $campaign = Campaign::find( $cid)->name;
        } else {
            $campaign ="";
        }
        
        $form->display("活動")->default($campaign)->readonly();
        
        $dateStr = "";
        if (request()->startdate &&  request()->enddate) {
            $dateStr =  request()->startdate." ~ " . request()->enddate;
        } else if (request()->startdate) {
            $dateStr = request()->startdate;
        } else if (request()->enddate) {
            $dateStr = request()->enddate;
        }
        
        $typeText = "全部";
        if (request()->type ==1) {
            $typeText = "合格";
        } else if (request()->type ==2) {
            $typeText = "不合格";
        }
        
        $form->display("合格與不合格" ) ->default($typeText)->readonly();
        $form->display("店訪日期" ) ->default($dateStr)->readonly();
        
        
        //$form->dateRange('startdate2', 'enddate2',"結束日期區間")->readonly();;
      
        $form->addButton("back");
        
        $grid = new Grid(new Answer);
        
        //加入匯出功能按鈕
        $grid->tools(function (Grid\Tools $tools) {
            $tools->append(new ToolAnalyzeExportData("匯出","明細報表"));
            //$tools->append(new ToolExportPassData("匯出"));
        });
        
        $grid->disableActions();
        $grid->disableCreateButton();
        $grid->disableFilter();
        
        $grid->disableRowSelector();
        $grid->disableColumnSelector();
        $grid->disableExport();
        $model = $grid->model();
        
        $model->select('answers.score as answer_score','answers.*', 'campaigns.*', 'stores.*');
        $model->join('campaigns', 'campaigns.id', '=', 'answers.campaign_id');
        $model->join('stores', 'stores.store_code', '=', 'answers.store_code');
        
        $model->where("answers.status" , 2);
        $model->whereIn("answers.access_status" , [0,1]);
        
        //$grid->model()->orderByRaw("field(stores.area ,'北一區', '北二區','中區','南區')", 'asc');
        $grid->model()->orderByRaw("field(stores.area ,'北一區', '北二區','中區','南區') asc, campaigns.startdate desc , answer_score desc" );
       
        
       
        if (request()->path_partner_no) {
            $model->where("stores.path_partner_no",   request()->path_partner_no);
        } else {
             
            
             if(request()->area){ 
                
                 $aeraSql ="";
                  
                 if (request()->area !="全國") {
                     $model->where("stores.area",  request()->area);
                 } else {
                     if (!$cid)  {
                          $model->whereRaw("campaigns.id = (SELECT campaign_id from answers where status =2 and  access_status in(0,1)  order by access_date desc LIMIT  1)");
                     }  
                 }
             }
        }
        
        if (request()->type ==1) { //合格
            $model->whereRaw("answers.score >= campaigns.score");
        } else if (request()->type ==2) {//不合格
            $model->whereRaw("answers.score < campaigns.score");
        }
        
        if ($cid ) {
             
           // $str = implode("," , $cid);
            $model->where("campaigns.id",   $cid);
            
        }
        
        
        if (request()->startdate) {
            $model->where("answers.access_date", ">=" , request()->startdate);
        }
        
        if (request()->enddate) {
            $model->where("answers.access_date", "<=" , request()->enddate);
        }
      
        
         /*
        $grid->export(function ($export) {
            
            $export->filename('統計明細報表');
            
            $export->except(['img_path', 'recording_path']);
          
        });*/
          
        $grid->column('campaign.name' , "活動名稱");
        $grid->column('store.area' , "分區");
        $grid->column('store.path_partner_no' , "TP");
        $grid->column('store.fma' , "FMA");
        $grid->column('store.sales_area_no' , "TPSR");
        $grid->column('store.store_no' , "客戶店號");
        $grid->column('store.name' , "店家名稱");
         
        $grid->column('access_status' , "店訪狀態")->using(Answer::ACCESS_STATUS);
        $grid->column('store.area_name' , "縣市");
       
        $grid->column('voucher_type' , "禮卷類別")->display(function($voucher_type)  {
                return  Campaign::VOUCHAR_ALL_TYPE[$voucher_type];
        });
        
        $grid->column('voucher' , "發放禮卷金額");
        
        $grid->column('answer_score' , "得分")->display(function($answer_score)  {
            return $this->getAnswerScore();
        });
      
        $grid->column("img_path",'照片')->display(function($img_path)  {
            if (!$img_path) return "尚無檔案";
            
            $url = config("filesystems.disks.admin.url");
            $arr = explode(".", $img_path);
            $path = $arr[0].".".$arr[1];
            return  "
            <a href='$url/$img_path' target='_blank'>
                         開啟
            </a>
            ";
        });
            
        $grid->column("recording_path",'錄音檔')->display(function($recording_path)  {
                if (!$recording_path) return "尚無檔案";
                
                $url = config("filesystems.disks.admin.url");
                
                return  "
                <a href='$url/$recording_path' target='_blank'>
                播放
                </a>
                ";
         });
        
        //$grid->column('reviewUser.name' , "審核者");
        $grid->column('access_date' , "訪查時間");
        
    
        $box = new Box("",$form  );
         
        
        $str = $grid->render();
        //dd(Db::getQueryLog());
        return $content->header("明細報表")->body($box)->body($str) ;
    }
    
    public function getData() {
        //DB::enableQueryLog();
        $where ="";
        $whereData =[];
        $sql_pass = "";
        $sql_nopass = "";
        
        $area_tyep = request()->area_tyep ;
         
        $cid= request()->cid;
        /*
        if (request()->no) {
            $where .= " and c.no = ?";
            $whereData[] =  request()->no;
        }
        
        if (request()->cid) {
            $where .= " and c.id = ?";
            $whereData[] =  request()->cid;
        }
        */
        if ($cid) {
            $where .= " and c.id = $cid";
        }
         
        if (request()->startdate) {
            $where .= " and  a.access_date >= ?";
            $whereData[] = request()->startdate;
        }
       
     
        if (request()->enddate) {
            $where .= " and  a.access_date <= ?";
            $whereData[] = request()->enddate;
        }
        
        
        if (request()->group ==1 || request()->group == null) {
            $aeraSql ="";
          
            if ($area_tyep == null || $area_tyep==0) {
                if (strlen($where)  ==0 && sizeof($whereData) == 0 ) {
                    $aeraSql = "and c.id = (SELECT campaign_id from answers where status =2 and  access_status in(0,1)  order by access_date desc LIMIT  1)";
                } 
              
            } else {
                if (request()->area_tyep !=0) {
                   $tempArrayName = Campaign::AREA[$area_tyep] ;
                   $aeraSql = "and  s.area = '$tempArrayName'";
                    
                }
                
            }
            
            //合格
            $sql_pass = "
            select  c.name ,  s.area area_tyep ,  count(1) answre_count, sum(a.score) score
            from campaigns c , answers a , stores s
            where c.id = a.campaign_id
            and  a.score >= c.score
            and a.store_code = s.store_code
            and a.status =2 and a.access_status in(0,1)
            $aeraSql
            $where
            group by s.area , c.name
            
            ";
         
            
            //不合格
            $sql_nopass = "
            select   c.name ,  s.area area_tyep ,  count(1) answre_count, sum(a.score) score
            from campaigns c , answers a , stores s
            where c.id = a.campaign_id
            and  a.score < c.score
            and a.store_code = s.store_code
            and a.status =2  and a.access_status in(0,1)
            $aeraSql
            $where
            group BY s.area , c.name
            ";
            
        }  else {
           
            $aeraSql ="";
            
            if ($area_tyep == null || request()->area_tyep ==0) {
                if ( strlen($where)  ==0 && sizeof($whereData) == 0 ) {
                    $aeraSql = "and c.id = (SELECT campaign_id from answers where status =2 and  access_status in(0,1)  order by access_date desc LIMIT  1)";
                }
            } else {
                if (request()->area_tyep !=0) {
                    $tempArrayName = Campaign::AREA[$area_tyep] ;
                    $aeraSql = "and  s.area = '$tempArrayName'";
                }
            }
            
            //合作通路不合格
            $sql_pass = "
                select  c.name, path_partner_no area_tyep,   count(1) answre_count , sum(a.score) score
                from campaigns c , answers a , stores s
                where c.id = a.campaign_id 
                and   a.store_code = s.store_code
                and   a.score >= c.score
                and a.status =2 and a.access_status in(0,1) 
                $aeraSql
                $where 
                GROUP BY   path_partner_no  ,  c.name
            ";
            
            //合作通路布不合格
            $sql_nopass = "
                select  c.name,  path_partner_no area_tyep,   count(1) answre_count , sum(a.score) score
                from campaigns c , answers a , stores s
                where c.id = a.campaign_id 
                and   a.store_code = s.store_code
                and   a.score < c.score
                and   a.status =2 and a.access_status in(0,1) 
                $aeraSql
                $where 
                GROUP BY   path_partner_no  , c.name
            ";
        }
        
       
        $pass =  DB::select($sql_pass, $whereData);
        
        $nopass =  DB::select($sql_nopass, $whereData);
        
        
        /*
        $cid = request()->cid;
        
        if (!$cid) {
           $tmep =  DB::select("select id   from  campaigns  where startdate < now() order by startdate  desc LIMIT 1");
           if (sizeof($tmep) > 0) {
               $cid = $tmep[0]->id;
            }
        }
        */
        //結合資料
        $result = [];
        $url =  url("/admin/chart/detail?").request()->getQueryString();
      
        if ((request()->group ==1 || request()->group == null)  && ( request()->area_tyep ==0 || request()->area_tyep== null)) {
            $result["全國"] = [ 
                "name" => "",
                "area_name" =>  "全國",
                "area_tyep"=> "全國",
                "pass" =>  0,
                "nopass" => 0,
                "answre_count"=> 0,
                "on_answre_count"=> 0,
                "url"=> $url."&area=全國",
            ];
        }   
       
        foreach ($pass as $row) {
            $queryString ="";
            if (request()->group==2) {
                $area_name =  "通路".$row->area_tyep;
                $queryString = "&path_partner_no=$row->area_tyep";
            } else {
                $area_name =   $row->area_tyep ;
                $queryString = "&area=$row->area_tyep";
            }
            
            $result[$row->area_tyep] = [
                "name" => $row->name,
                "area_name" =>  $area_name,
                "area_tyep"=>$row->area_tyep,
                "pass" =>   $row->score,
                "nopass" => 0,
                "answre_count"=> $row->answre_count,
                "on_answre_count"=> 0,
                "url"=> $url.$queryString,
            ];
            
            if ((request()->group ==1 || request()->group == null)  && ( request()->area_tyep ==0 || request()->area_tyep== null)) {
                $all = $result["全國"];
                $all["name"] = $row->name;
                $all["pass"] += $row->score;
                $all["answre_count"] += $row->answre_count;
                $result["全國"] = $all;
              
            }
        }
        
      
        foreach ($nopass as $row) {
            $queryString ="";
            if (request()->group==2) {
                $area_name =  "通路".$row->area_tyep;
                $queryString = "&path_partner_no=$row->area_tyep";
            } else {
                $area_name =   $row->area_tyep ;
                $queryString = "&area=$row->area_tyep";
            }
           
            
            if (isset($result[$row->area_tyep])) {
                $temp = $result[$row->area_tyep];
                $temp["nopass"] = $row->score;
                $temp["on_answre_count"] = $row->answre_count;
            }  else {
                $temp = [];
                $temp["name"] = $row->name;
                $temp["area_name"] = $area_name;
                $temp["area_tyep"] = $row->area_tyep;
                $temp["pass"] =0;
                $temp["nopass"] = $row->score;
                $temp["answre_count"] =0;
                $temp["on_answre_count"] = $row->answre_count;
                $temp["url"] =  $url.$queryString;
            }
             
            
            if ((request()->group ==1 || request()->group == null)  && ( request()->area_tyep ==0 || request()->area_tyep== null)) {
                $all = $result["全國"];
                $all["name"] = $row->name;
                $all["nopass"] += $row->score;
                $all["on_answre_count"] += $row->answre_count;
                $result["全國"] = $all;
            }
            
            $result[$row->area_tyep] = $temp;
        }
        
       
        if (request()->area_tyep > 0  ){
            $result = collect($result)->sortBy('area_tyep')->toArray();
        }
         
        $array =[];
        foreach ($result as $row) {
            $array[] = $row;
          
        }
       
        return $array;
    }
    
    
    public function form() {
        $form = new Form(request()->all());
        $form->disableReset();
        $form->method("GET");
        $form->action(url('admin/chart'));
        
        //$form->text("no" , "活動編號");
        
       
        $form->html("* 地區全國 代表選擇所有區域統計。若未選擇過濾條件，預設統計有最新訪店資料的活動");
        $form->radio("area_tyep","地區")->options(Campaign::AREA);
        $form->select("cid", "活動")->options(Campaign::orderby("startdate", "desc")->pluck('name', 'id'));
        $form->dateRange('startdate', 'enddate',"店訪日期");
        $form->radio("group", "統計規則")->options([
            1=>"合計",
            2=>"通路"
            
        ])->default('1');;
       
        return $form;
        
    }
    
    public function removeArrNull($arr) {
        if (!$arr) return [];
       
        $n =0;
        foreach ($arr as $val) {
            if ($val ==null) {
                unset($arr[$n]);
                break;
            }
            $n++;
        }
        
        return $arr;
    }
}
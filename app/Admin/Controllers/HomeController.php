<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\Dashboard;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;

class HomeController extends Controller
{
    public function index(Content $content)
    {
       
       if (auth()->user()->roles()->whereIn("slug",["customer"])->exists()) {
           return redirect("admin/chart");
       }
        
       return redirect("admin/campaign");
        
        //->title(auth()->user()->name.' 您好， 歡迎使用創心研究秘密客分析系統')
        ;
             
    }
}

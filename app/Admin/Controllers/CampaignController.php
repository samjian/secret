<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Campaign;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Grid\Displayers\ContextMenuActions;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use App\Models\Answer;
Use Encore\Admin\Admin;

class CampaignController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header("活動")
            ->description("管理")
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header("活動")
            ->description("檢視")
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
        ->header("活動")
        ->description("編輯")
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
        ->header("活動")
        ->description("建立")
            ->body($this->form());
    }

    /**
     * 管理者建立及管理活動 清單
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Campaign);
        $grid->model()->selectRaw("campaigns.*, (select count(1) from answers where answers.campaign_id = campaigns.id and status=1 ) as review_count");
        $grid->model()->orderBy('review_count', 'desc');
        $grid->model()->orderBy('created_at', 'asc');
       
        $grid->disableExport();
        $grid->disableRowSelector();
         
        $grid->actions(function ($actions) {
            // 去掉编辑
            $actions->disableView();
            $actions->disableDelete();
            
        }); 
        
        $user = auth()->user();
        
        $isManager= $user->roles()->whereIn("slug",["manager", "approver"])->exists() || $user->isAdministrator() ;
        //$isManager= $user->roles()->where("slug","")->exists();
        
        $grid->no('活動編號');
        $grid->name('活動名稱');
        
        $grid->column('area_tyep', '地區')->display(function ($area_tyep) {
            return Campaign::AREA[$area_tyep];
        });
            
            
        //  管理者顯示所有活動 非管理者顯示以上線活動
        if (  $isManager  ) {
            
            if ($user->roles->first()->slug == "approver") {
                $start_date = date('Y-m-d');
                
                //活動結束日期截止後七日即無法填單
                $end_date = date('Y-m-d' ,   strtotime("-7 days", time()));
                
                $grid->model()->whereRaw("'$start_date' >= startdate   && '$end_date' <= enddate ");
            }
            
             
            $grid->review_count('待審核筆數'); 
                
            $grid->column('審核答卷')->display(function($id) {
                return "<a href='".url("/admin/answer?c=$this->id")."'>進入審核清單</a>";
            });
            $grid->startdate('起始日期');
            $grid->enddate('結束日期');
            
            $grid->created_at(trans('admin.created_at'));
            $grid->updated_at("最後儲存日期");
        } else {
       
            $start_date = date('Y-m-d');
            
            //活動結束日期截止後三日即無法填單
            $end_date = date('Y-m-d' ,   strtotime("-3 days", time()));
             
            $grid->model()->whereRaw("'$start_date' >= startdate   && '$end_date' <= enddate ");
            
            $grid->disableCreateButton();
            $grid->disableActions();
            
            $grid->column('問卷管理')->display(function ($id) {
                return "<a href='".url("/admin/answer?c=$this->id")."'>開啟</a>";
            });
                
            /*
            $grid->column('改已送出問卷')->display(function($id) {
                return " 修改 ";
            });
            */
            
            $grid->startdate('起始日期');
            $grid->enddate('結束日期');
            
            $grid->created_at(trans('admin.created_at'));
            $grid->updated_at("最後儲存日期");
        }
          
         
        $grid->filter(function($filter){
            $filter->disableIdFilter();
            $filter->contains('no',"活動編號");
            $filter->contains('name',"活動名稱");
            $filter->equal('area_tyep', "地區")->radio(Campaign::AREA );
            $filter->between("startdate","起始日期區間")->date();
            $filter->between('enddate',"結束日期區間")->date();
        });
            
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Campaign::findOrFail($id));

        $show->id('ID');
        $show->no('no');
        $show->name('name');
        $show->startdate('startdate');
        $show->enddate('enddate');
        $show->area_tyep('area_tyep');
        $show->is_voucher('is_voucher');
        $show->topic_0('topic_0');
        $show->topic_0_a('topic_0_a');
        $show->topic_0_b('topic_0_b');
        $show->topic_0_c('topic_0_c');
        $show->topic_0_d('topic_0_d');
        $show->topic_0_e('topic_0_e');
        $show->topic_0_f('topic_0_f');
        $show->topic_0_g('topic_0_g');
        $show->topic_0_h('topic_0_h');
        $show->topic_0_i('topic_0_i');
        $show->topic_0_j('topic_0_j');
        
        $show->topic_1('topic_1');
        $show->topic_1_answer('topic_1_answer');
        $show->topic_1_a('topic_1_a');
        $show->topic_1_b('topic_1_b');
        $show->topic_1_c('topic_1_c');
        $show->topic_1_d('topic_1_d');
        $show->topic_1_e('topic_1_e');
        $show->topic_1_f('topic_1_f');
        $show->topic_1_g('topic_1_g');
        $show->topic_1_h('topic_1_h');
        $show->topic_1_i('topic_1_i');
        $show->topic_1_j('topic_1_j');
        
        $show->topic_2('topic_2');
        $show->topic_2_answer('topic_2_answer');
        $show->topic_2_a('topic_2_a');
        $show->topic_2_b('topic_2_b');
        $show->topic_2_c('topic_2_c');
        $show->topic_2_d('topic_2_d');
        $show->topic_2_e('topic_2_e');
        $show->topic_2_f('topic_2_f');
        $show->topic_2_g('topic_2_g');
        $show->topic_2_h('topic_2_h');
        $show->topic_2_i('topic_2_i');
        $show->topic_2_j('topic_2_j');
        
        $show->topic_3('topic_3');
        $show->topic_3_answer('topic_3_answer');
        $show->topic_3_a('topic_3_a');
        $show->topic_3_b('topic_3_b');
        $show->topic_3_c('topic_3_c');
        $show->topic_3_d('topic_3_d');
        $show->topic_3_e('topic_3_e');
        $show->topic_3_f('topic_3_f');
        $show->topic_3_g('topic_3_g');
        $show->topic_3_h('topic_3_h');
        $show->topic_3_i('topic_3_i');
        $show->topic_3_j('topic_3_j');
        
        /*
        $show->topic_4('topic_4');
        $show->topic_4_answer('topic_4_answer');
        $show->topic_4_a('topic_4_a');
        $show->topic_4_b('topic_4_b');
        $show->topic_4_c('topic_4_c');
        $show->topic_4_d('topic_4_d');
        $show->topic_4_e('topic_4_e');
        $show->topic_4_f('topic_4_f');
        $show->topic_4_g('topic_4_g');
        $show->topic_4_h('topic_4s_h');
       */

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Campaign);
        /*
        if ($form->isCreating()) {
            $form->builder()->getFooter()->checkCreating();
        } else {
            $form->builder()->getFooter()->checkEditing();
        }*/
       
        
        $form->tools(function (Form\Tools $tools) {
            $tools->disableView();
            $tools->disableDelete();
        });
        
        $form->footer(function ($footer) {
            $footer->disableViewCheck();
        });
             
        
        $no = chr(rand(0, 25) + 65);
    
        for($i = 0; $i < 6; $i++){
            $no = $no."".rand(0, 9);
        }
        $form->hidden('no')->value($no);
            
        
        $form->fieldset('活動設定', function (Form $form) {
            $form->text('name', '活動名稱')->required();
            $form->dateRange('startdate', 'enddate',"起迄日期") ->required();
            
            $form->radio("area_tyep","適用地區")
                 ->options(Campaign::AREA)
                 ->default(Campaign::AREA[0])->required();
                
            
            $form->radio('is_voucher', '現場發禮卷')
                 ->options([1=>"有", 0=>"無"])
                 ->default(0)->required()
                 ->when(1, function (Form $form) { 
                     $form->select('voucher_type', '禮劵類別')->options(Campaign::VOUCHAR_TYPE);
                 })->when(0, function (Form $form) { 
                     
                 });
            
            $form->number('score', '合格分數')->min(0)->default(0)->required();
        });
       
          
          
        $form->fieldset('直接淘汰題', function (Form $form) {
            $form->textarea('topic_0', '淘汰標準說明');
            $form->text('topic_0_a', '淘汰標準A');
          
            $form->text('topic_0_b', '淘汰標準B');
           
            $form->text('topic_0_c', '淘汰標準C');
         
            $form->text('topic_0_d', '淘汰標準D');
           
            $form->text('topic_0_e', '淘汰標準E');
           
            $form->text('topic_0_f', '淘汰標準F');
           
            $form->text('topic_0_g', '淘汰標準G');
          
            $form->text('topic_0_h', '淘汰標準H');
            
            $form->text('topic_0_i', '淘汰標準I');
            
            $form->text('topic_0_j', '淘汰標準J');
            
        });
              
      
            
           
        $form->fieldset('問卷題目一', function (Form $form) {
            
            $form->textarea('topic_1', '題目');
             
            $form->radio('topic_1_answer', '店家回應')
                  ->options([1=>"是， 需輸入店家回應", 0=>"否，直接評分即可"])
                  ->default(0);
            $form->radio('topic_1_type', '題目類型')->options([0=>"單選", 1=>"複選題"])->default(0);
            $form->text('topic_1_a', '回答A');
            $form->number('topic_1_a_score', '分數')->min(0);
            $form->text('topic_1_b', '回答B');
            $form->number('topic_1_b_score', '分數')->min(0);
            $form->text('topic_1_c', '回答C');
            $form->number('topic_1_c_score', '分數')->min(0);
            $form->text('topic_1_d', '回答D');
            $form->number('topic_1_d_score', '分數')->min(0);
            $form->text('topic_1_e', '回答E');
            $form->number('topic_1_e_score', '分數')->min(0);
            $form->text('topic_1_f', '回答F');
            $form->number('topic_1_f_score', '分數')->min(0);
            $form->text('topic_1_g', '回答G');
            $form->number('topic_1_g_score', '分數')->min(0);
            $form->text('topic_1_h', '回答H');
            $form->number('topic_1_h_score', '分數')->min(0);
            
            $form->text('topic_1_i', '回答I');
            $form->number('topic_1_i_score', '分數')->min(0);
            $form->text('topic_1_j', '回答J');
            $form->number('topic_1_j_score', '分數')->min(0);
        });
     
        $form->fieldset('問卷題目二', function (Form $form) {
             
            $form->textarea('topic_2', '題目');
             
            $form->radio('topic_2_answer', '店家回應')
                 ->options([1=>"是， 需輸入店家回應", 0=>"否，直接評分即可"])
                  ->default(0);
            
            $form->radio('topic_2_type', '題目類型')->options([0=>"單選", 1=>"複選題"])->default(0);
            $form->text('topic_2_a', '回答A');
            $form->number('topic_2_a_score', '分數')->min(0);
            $form->text('topic_2_b', '回答B');
            $form->number('topic_2_b_score', '分數')->min(0);
            $form->text('topic_2_c', '回答C');
            $form->number('topic_2_c_score', '分數')->min(0);
            $form->text('topic_2_d', '回答D');
            $form->number('topic_2_d_score', '分數')->min(0);
            $form->text('topic_2_e', '回答E');
            $form->number('topic_2_e_score', '分數')->min(0);
            $form->text('topic_2_f', '回答F');
            $form->number('topic_2_f_score', '分數')->min(0);
            $form->text('topic_2_g', '回答G');
            $form->number('topic_2_g_score', '分數')->min(0);
            $form->text('topic_2_h', '回答H');
            $form->number('topic_2_h_score', '分數')->min(0);
            
            $form->text('topic_2_i', '回答I');
            $form->number('topic_2_i_score', '分數')->min(0);
            $form->text('topic_2_j', '回答J');
            $form->number('topic_2_j_score', '分數')->min(0);
            
        });
       
   
        $form->fieldset('問卷題目三', function (Form $form) {
            
            
            $form->textarea('topic_3', '題目');
            
            
            $form->radio('topic_3_answer', '店家回應')
                 ->options([1=>"是， 需輸入店家回應", 0=>"否，直接評分即可"])
                 ->default(0);
            
            $form->radio('topic_3_type', '題目類型')->options([0=>"單選", 1=>"複選題"])->default(0);
            $form->text('topic_3_a', '回答A');
            $form->number('topic_3_a_score', '分數')->min(0);
            $form->text('topic_3_b', '回答B');
            $form->number('topic_3_b_score', '分數')->min(0);
            $form->text('topic_3_c', '回答C');
            $form->number('topic_3_c_score', '分數')->min(0);
            $form->text('topic_3_d', '回答D');
            $form->number('topic_3_d_score', '分數')->min(0);
            $form->text('topic_3_e', '回答E');
            $form->number('topic_3_e_score', '分數')->min(0);
            $form->text('topic_3_f', '回答F');
            $form->number('topic_3_f_score', '分數')->min(0);
            $form->text('topic_3_g', '回答G');
            $form->number('topic_3_g_score', '分數')->min(0);
            $form->text('topic_3_h', '回答H');
            $form->number('topic_3_h_score', '分數')->min(0);
            
            $form->text('topic_3_i', '回答I');
            $form->number('topic_3_i_score', '分數')->min(0);
            $form->text('topic_3_j', '回答J');
            $form->number('topic_3_j_score', '分數')->min(0);
            
        });
        
        
        $form->fieldset('問卷題目四 (不列入評分)', function (Form $form) {
            
            
            $form->textarea('topic_4', '題目');
            
            
            $form->radio('topic_4_answer', '店家回應')
            ->options([1=>"是， 需輸入店家回應", 0=>"否，直接評分即可"])
            ->default(0);
            
            
            $form->text('topic_4_a', '回答A');
            
            $form->text('topic_4_b', '回答B');
            
            $form->text('topic_4_c', '回答C');
            
            $form->text('topic_4_d', '回答D');
            
            $form->text('topic_4_e', '回答E');
            
            $form->text('topic_4_f', '回答F');
            
            $form->text('topic_4_g', '回答G');
            
            $form->text('topic_4_h', '回答H');
          
            $form->text('topic_4_i', '回答I');
            
            $form->text('topic_4_j', '回答J');
        });
        
            
        $form->fieldset('開放回應題目', function (Form $form) {
            $form->text('topic_open_1', '開放回應題目一');
            $form->text('topic_open_2', '開放回應題目二');
        });
        
        $form->fieldset('菸品條碼選項', function (Form $form) {
            $form->text('tobacco_code_1', '菸品條碼選項一');
            $form->text('tobacco_code_2', '菸品條碼選項二');
            $form->text('tobacco_code_3', '菸品條碼選項三');
            $form->text('tobacco_code_4', '菸品條碼選項四');
            $form->text('tobacco_code_5', '菸品條碼選項五');
            $form->text('tobacco_code_6', '菸品條碼選項六');
             
                
       });
        
        
        $form->saving(function (Form $form) {
           
            if ($form->is_voucher ==0 ) {
                
                $form ->voucher_type =0;
            }
           // dd(  $form ->voucher_type ,$form );
        });
         
        return $form;
    }
}

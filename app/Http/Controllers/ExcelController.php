<?php

namespace  App\Http\Controllers;

use App\Admin\Export\AnswerExporter;
use App\Admin\Export\AnalyzeExporter;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Campaign;
use App\Util\StoreImport;

class ExcelController extends Controller
{
    public function import() {
        Excel::import(new StoreImport, 'store.xlsx');
    }
   
    public function answerAll($campaignId,Request $request) {
       
        $date = date("Ymd");
        return Excel::download(new AnswerExporter($campaignId), "所有答卷資料_$date.xlsx" );
    }
    
    public function answerPass(Request $request) {
        $date = date("Ymd");
        $area = $request->area_tyep ?  Campaign::AREA[ $request->area_tyep] : $request->area;
        
        $campaign = Campaign::find($request->cid);
       
        return Excel::download(new AnswerExporter($request->cid, 2, $area), $campaign->name."_$date.xlsx");
    }
   
    public function analyzeExportData(Request $request) {
        $date = date("Ymd");
        return Excel::download(new AnalyzeExporter(), $request->file_name."_$date.xlsx");
    }
}

<?php

namespace App\Models;



use Encore\Admin\Auth\Database\Administrator;

class Answer extends BaseModel
{
    public const ACCESS_STATUS = [
        "營業 (第一次拜訪)", "營業 (第二次拜訪)","未營業 (第二次拜訪)","結束營業"
    ];
    
    public const STATUS = [
         "暫存", "送審" , "審核通過"  , "不通過"
    ];
    
    
   
    
    public function getTopic1AnswersAttribute($value)
    {
        return explode(',', $value);
    }
    
    public function setTopic1AnswersAttribute($value)
    {
        if(is_array($value)) {
            $this->attributes['topic_1_answers'] = implode(',', $value);
        } else {
            $this->attributes['topic_1_answers'] =  $value ;
        }
    }
    
    
    public function getTopic2AnswersAttribute($value)
    {
        return explode(',', $value);
    }
    
    public function setTopic2AnswersAttribute($value)
    { 
        if(is_array($value)) {
            $this->attributes['topic_2_answers'] = implode(',', $value);
        } else {
            $this->attributes['topic_2_answers'] =  $value ;
        }
    }
    
    public function getTopic3AnswersAttribute($value)
    {
        return explode(',', $value);
    }
    
    public function setTopic3AnswersAttribute($value)
    {
        if(is_array($value)) {
            $this->attributes['topic_3_answers'] = implode(',', $value);
        } else {
            $this->attributes['topic_3_answers'] =  $value ;
        }
    }
    
    
    public function getAnswerScore() {
        $answer_score = "不計分";
        
        if ($this->access_status ==0 || $this->access_status ==1) {
            $answer_score = $this->answer_score;
        }  
        
        return $answer_score;
    }
    
    public function campaign()
    {
        return $this->belongsTo(Campaign::class,'campaign_id');
    }
    
    /**
     * 菸品回應 1
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function products1()
    {
        return $this->morphToMany(Product::class, 'taggable',"taggables_1", "tag_id" , "taggable_id");
    }
    
    /**
     * 菸品回應 2
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function products2()
    {
        return $this->morphToMany(Product::class, 'taggable',"taggables_2", "tag_id" , "taggable_id");
    }
    
    /**
     * 菸品回應 3
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function products3()
    {
        return $this->morphToMany(Product::class, 'taggable',"taggables_3", "tag_id" , "taggable_id");
    }
    
    /**
     * 菸品回應 4
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function products4()
    {
        return $this->morphToMany(Product::class, 'taggable',"taggables_4", "tag_id" , "taggable_id");
    }
    
    public function store()
    {
        return $this->belongsTo(Store::class,'store_code' , "store_code");
    }
    
    /**
     * 填單者
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->belongsTo(Administrator::class,'admin_user_id');
    }
    
    /**
     * 審核者
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function reviewUser()
    {
        return $this->belongsTo(Administrator::class,'review_user_id');
    }
    
    
   
    
}

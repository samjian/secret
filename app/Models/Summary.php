<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;

class Summary extends Model
{
    public function paginate()
    {
        $perPage = Request::get('per_page', 10);
        
        $page = Request::get('page', 1);
       
        $no= $this->removeArrNull(request()->no);
        $cid= $this->removeArrNull(request()->cid);
        
        $area_tyep = request()->area_tyep;
       
        
        $where="";
        $sub_where="";
        $whereData=[];
       
      
        if(request()->area_tyep > 0){
            $where .= " and  s.area  = ?";
           
            $whereData[] =  Campaign::AREA[request()->area_tyep];
        }    
         
        
        if (sizeof($no) > 0) {
            $str = implode("','" , $no);
            $where .= " and c.no in ('$str')";
        }
        
        
        if (sizeof($cid) > 0) {
            $str = implode("," , $cid);
            $where .= " and c.id in ($str)";
            
        }
        
        $date =  request()->date;
        
        if ($date && $date["start"]) {
            $startdate = $date["start"];
            $where .= " and  a.access_date >= ?";
            $sub_where =" and  sub_a.access_date  >= '$startdate'";
            $whereData[] =$date["start"];
        }
        
        if ($date && $date["end"]) {
            $enddate = $date["end"];
            $where .= " and   a.access_date <= ?";
            $sub_where .= " and  sub_a.access_date <= '$enddate'";
            $whereData[] =$date["end"];
        }
        
        
        /*
        if (request()->area_tyep == null && sizeof($whereData) == 0 ) {
           
            $where .= "and c.id = (select c.id from answers a,  campaigns c   
                            where a.campaign_id = c.id 
                            and  c.startdate < now()
                            order by c.startdate  desc LIMIT 1
                        )";
        }*/
        
        $start = ($page-1)*$perPage;
        $end = $start + $perPage;
       
        $sql = "
                select 
                    c.no,
                    c.name,
                    s.area , 
                    s.path_partner_no ,
			 
				count(a.store_code) store_count,
			    (
				 	  select count(1) from  answers sub_a, stores sub_s  
                     where sub_a.store_code = sub_s.store_code and sub_a.access_status=2 and sub_s.path_partner_no = s.path_partner_no and sub_a.campaign_id = c.id and sub_a.status =2
                    $sub_where
			    ) as two_access ,
			    (
				    select count(1) from  answers sub_a, stores sub_s  
				  	where sub_a.store_code = sub_s.store_code and sub_s.path_partner_no = s.path_partner_no 
					   and sub_a.topic_1_answers_score > 0 and sub_a.campaign_id = c.id and sub_a.status =2  and sub_a.access_status in(0,1) 
                    $sub_where
				) topic_1_score_count,
				(
				    select count(1) from  answers sub_a, stores sub_s  
				  	where sub_a.store_code = sub_s.store_code and sub_s.path_partner_no = s.path_partner_no 
					   and sub_a.topic_2_answers_score > 0 and sub_a.campaign_id = c.id and sub_a.status =2  and sub_a.access_status in(0,1) 
                    $sub_where
				) topic_2_score_count,
				(
				    select count(1) from  answers sub_a, stores sub_s  
				  	where sub_a.store_code = sub_s.store_code and sub_s.path_partner_no = s.path_partner_no 
					   and sub_a.topic_3_answers_score > 0 and sub_a.campaign_id = c.id and sub_a.status =2  and sub_a.access_status in(0,1) 
                    $sub_where
				) topic_3_score_count,
				
				CONCAT(FORMAT((
				    select count(1) from campaigns sub_c,  answers sub_a, stores sub_s  
				  	 where sub_c.id = sub_a.campaign_id and sub_a.store_code = sub_s.store_code and sub_s.path_partner_no = s.path_partner_no 
					   and sub_a.score > sub_c.score  and sub_c.id = c.id and sub_a.status =2  and sub_a.access_status in(0,1) 
                    $sub_where
				)  / count(1)  * 100 ,2), '%')  as pass
			 
            	from campaigns c , answers a , stores s
            	where c.id = a.campaign_id 
                and s.store_code = a.store_code
            	and a.status =2  
               
                $where
               
            	GROUP BY  c.id,c.no,c.name,s.area , s.path_partner_no 
                Order by    c.startdate desc ,  pass desc 
                LIMIT $start,$end
       ";
        
         
       $result = DB::select($sql, $whereData);
        
      
        $count = DB::select("
           select c.name,s.area, s.path_partner_no 
                from campaigns c , answers a , stores s
            	where c.id = a.campaign_id 
                and s.store_code = a.store_code
            	and a.status =2  
                $where
            	GROUP BY c.name,s.area , s.path_partner_no 
              
        ",$whereData) ;
         
        
        $datas = static::hydrate($result);
        
        
        $paginator = new LengthAwarePaginator($datas, count($count), $perPage);
        
        $paginator->setPath(url()->current());
        
        return $paginator;
    }
    
     
    // 覆盖`where`来收集筛选的字段和条件
    public function where($column, $operator = null, $value = null, $boolean = 'and')
    {
        
    } 
    
    public static function with($relations)
    {
        return new static;
    }
    
    public function removeArrNull($arr) {
        if (!$arr) return [];
        
        $n =0;
        foreach ($arr as $val) {
            if ($val ==null) {
                unset($arr[$n]);
                break;
            }
            $n++;
        }
        
        return $arr;
    }
}

<?php

namespace App\Models;
 
class Campaign extends BaseModel
{
    
    public const AREA = [
        0 => '全國',
        1=> '北一區',
        2=> '北二區' ,
        3=>"中區",
        4 => "南區"
    ];
    
    public const VOUCHAR_TYPE = [
            1=>"PX(全聯)",
            2=>"7-11(統一)",
            3=>"FM(全家)",
            4=>"HL(萊爾富)",
            5=>"OK(來來)",
            6=>"CF(家樂福)"
     ];
    
    public const VOUCHAR_ALL_TYPE = [
        0=>"無",
        1=>"PX(全聯)",
        2=>"7-11(統一)",
        3=>"FM(全家)",
        4=>"HL(萊爾富)",
        5=>"OK(來來)",
        6=>"CF(家樂福)"
    ];
    
    public function answers()
    {
        return $this->hasMany(Answer::class,'campaign_id');
    }
}

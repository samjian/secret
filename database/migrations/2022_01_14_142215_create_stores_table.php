<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_no');
            $table->string('name');
            $table->string('area_name');
            $table->string('addr');
            $table->string('addr_descript');
            $table->string('area');
            $table->string('path_partner_no')->comment('通路夥伴代碼');
            $table->string('jti_area_no')->comment('JTI分區代碼');
            $table->string('sales_area_no')->comment('業代區域代碼');
            $table->string('fma')->comment('FMA');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}

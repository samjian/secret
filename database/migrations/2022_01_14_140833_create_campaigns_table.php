<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no');
            $table->string('name');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('area_tyep')->comment('是用地區 1 全國 2 北一區3  北二區 4中區	5  南區');
            $table->integer('is_voucher')->comment('現場發禮卷');
            $table->string('topic_0')->nullable();
            $table->string('topic_0_a')->nullable();
            $table->string('topic_0_b')->nullable();
            $table->string('topic_0_c')->nullable();
            $table->string('topic_0_d')->nullable();
            $table->string('topic_0_e')->nullable();
            $table->string('topic_0_f')->nullable();
            $table->string('topic_0_g')->nullable();
            $table->string('topic_0_h')->nullable();
            $table->string('topic_1')->nullable();
            $table->integer('topic_1_answer')->comment('口  需輸入店家回應	1  不需輸入回應 0直接評分即可')->nullable();
            $table->string('topic_1_a')->nullable();
            $table->string('topic_1_b')->nullable();
            $table->string('topic_1_c')->nullable();
            $table->string('topic_1_d')->nullable();
            $table->string('topic_1_e')->nullable();
            $table->string('topic_1_f')->nullable();
            $table->string('topic_1_g')->nullable();
            $table->string('topic_1_h')->nullable();
            $table->string('topic_2')->nullable();
            $table->integer('topic_2_answer')->nullable();
            $table->string('topic_2_a')->nullable();
            $table->string('topic_2_b')->nullable();
            $table->string('topic_2_c')->nullable();
            $table->string('topic_2_d')->nullable();
            $table->string('topic_2_e')->nullable();
            $table->string('topic_2_f')->nullable();
            $table->string('topic_2_g')->nullable();
            $table->string('topic_2_h')->nullable();
            $table->string('topic_3')->nullable();
            $table->integer('topic_3_answer')->nullable();
            $table->string('topic_3_a')->nullable();
            $table->string('topic_3_b')->nullable();
            $table->string('topic_3_c')->nullable();
            $table->string('topic_3_d')->nullable();
            $table->string('topic_3_e')->nullable();
            $table->string('topic_3_f')->nullable();
            $table->string('topic_3_g')->nullable();
            $table->string('topic_3_h')->nullable();
            $table->string('topic_open_1')->nullable();
            $table->string('topic_open_2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('store_code')->unique()->comment('店家代碼')->nullable();
            $table->string('addr_memo')->comment('地址補充');
            $table->dateTime('access_date')->comment('店訪日期')->nullable();
            $table->string('access_status')->comment('電訪狀態')->nullable();
            $table->string('access_memo')->comment('電訪狀態說明');
            $table->string('topic_0_answers')->comment('直接淘汰提答案');
            $table->string('topic_1_answers')->comment('問題一回答');
            $table->string('topic_1_answers_product_id')->comment('第1層店家回應菸品');
            $table->string('topic_1_answers_other')->comment('第1層店家其他回應');
            $table->string('topic_1_answers_score')->comment('第1層提問評分');
            $table->string('topic_2_answers')->comment('問題2回答');
            $table->string('topic_2_answers_product_id')->comment('第2層店家回應菸品');
            $table->string('topic_2_answers_other')->comment('第2層店家其他回應');
            $table->string('topic_2_answers_score')->comment('第2層提問評分');
            $table->string('topic_3_answers')->comment('問題3回答');
            $table->string('topic_3_answers_product_id')->comment('第3層店家回應菸品');
            $table->string('topic_3_answers_other')->comment('第3層店家其他回應');
            $table->string('topic_3_answers_score')->comment('第3層提問評分');
            $table->integer('voucher')->comment('禮金');
            $table->string('topic_open_1_answers')->comment('開放問題一答案');
            $table->string('topic_open_2_answers')->comment('開放問題二答案');
            $table->string('tobacco_price')->comment('買回菸品價錢');
            $table->string('tobacco_code')->comment('買回菸品條碼');
            $table->string('tobacco_reason')->comment('未買回原因');
            $table->string('recording_path')->comment('錄音檔案路徑');
            $table->string('img_path')->comment('照片檔案');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers');
    }
}

<!-- prepare a DOM container with width and height -->
<div id="main" style="width: 100%;height:500px;"></div>
<script type="text/javascript">
	var myChart;
	window.onresize = function(event) {
		if (myChart) {
    		myChart.resize();
		}
    }

    $(function () {

 		var rows = {!! json_encode($datas) !!};
        
		var rows_arr = $.map( rows, function( row ) {
	    	  return  row ;
	    });
	 

	    var names = $.map( rows, function( row ) {
	    	  return  row.name ;
	    });
	 

	    var score = $.map( rows, function( row ) {
	    	  return  row.pass + row.nopass ;
	    });

	    var on_answre_count = $.map( rows, function( row ) {
	    	  return  row.on_answre_count ;
	    });


	    var answre_count = $.map( rows, function( row ) {
	    	  return  row.answre_count ;
	    });
	    
		 
 
        myChart = echarts.init(document.getElementById('main'));
        
        var  option =  {
      		 /*
      		  hover: {
  			    onHover: function(e) {
  			      $("#id-of-your-canvas").css("cursor", e[0] ? "pointer" : "default");
  			    }
  			  },*/
  			  tooltip: {
    				trigger: 'axis',
   				 
    			    formatter: function (datas, i) {
        			       
        			      var n  = datas[0].dataIndex;
        			        
        			      //rows = rows_arr[n];
 
		    	          return  name +
		    	       		   "<br>合格率 :" + datas[1].value.toFixed(2) + "%" + 
			    	       	   "<br>不合格率 :" + datas[0].value.toFixed(2) + "%" + 
		    	               "<br>合格店家數 :" + answre_count[n] +
		    	               "<br>不合格店家 :" + on_answre_count[n] 
    			    }
    		  },
  			  legend: {
    			    data: ['不合格', '合格']
    			  },
  			  xAxis: [
  			    {
  			      type: 'category',
  			      data: names
  			    
  			    }
  			  ],
  			  yAxis: {
   				  type: 'value',   
				  min: 0,
	   		      max: 100,
	   		      interval: 10,
	   		      axisLabel: {
	   		        formatter: '{value}%'
	   		      }
    		  },
  			  series: [
  			    {
    			   name: '不合格',
  			       type: 'bar',
    			   barWidth: 50,
    			   cursor: 'default',
    			   itemStyle:{
    				   color:"#ee6d61"
  				  },
  			      data: $.map( rows, function( row ) {
  			    	  return  row.on_answre_rate ;
    			  })
    		        
  			    },
			    {
			      name: '合格',
			      type: 'bar',
			      cursor: 'default',
			      itemStyle:{
				      color:"#45ba59"
				  },
			      barWidth: 50,
			      data:  $.map( rows, function( row ) {
			    	  return  row.answre_rate ;
				  })
			    },
  			  ],
  			  
  			};


        // use configuration item and data specified to show chart
        myChart.setOption(option );
        
    });
</script>
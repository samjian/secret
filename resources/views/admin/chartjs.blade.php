<!-- prepare a DOM container with width and height -->
<div id="main" style="width: 100%;height:850px;"></div>

<div  style="text-align: center;">
	 <font  color="#2894FF" size="6"> 80%~100%以上綠色 &nbsp; &nbsp; &nbsp;60%~79%黃色&nbsp; &nbsp; &nbsp;低於60%紅色</font> 
</div>


<script type="text/javascript">
	var myChart;
	window.onresize = function(event) {
		if (myChart) {
    		myChart.resize();
		}
    }

    $(function () {

 		var rows = {!! json_encode($datas) !!};
		 
		var title = [ {
		      text: rows.length > 0 ? rows[0].name : "",
/*
		      subtext: '80%~100%以上綠色        60%~79%黃色        低於60%紅色',
		      subtextStyle :{
	       	       color : '#272727'
	   	      },
			   	      */
		      left: 'center',
	   }];
		
		var datas = [];
		
		var title_x = [
			20,50,80,
			20,50,80,
			20,50,80,
		]; 

		var title_y = [
			20,20,20,
			50,50,50,
			80,80,80,
		]; 

		var data_x = [
			20,50,80,
			20,50,80,
			20,50,80,
		]; 

		var data_y = [
			25,25,25,
			55,55,55,
			85,85,85,
		]; 
		 
 	    for(var n=1,i=0; i<rows.length;i++,n++) {
 	 	    var row = rows[i];

 	 	     
		    
 	    	title[n]={
    			  text:  row.area_name,
    			 
        	      //subtext:"明細",//主標題的副標題文字內容，如果需要副標題就配置這一項
        	     // subtarget : 'self',
        	    
        	      //subtextStyle :{ color : '#66B3FF'},
    			  link : row.url,
        	      target :'self',
        	      fontSize : 12,
          	      sublink: row.url,//點選副標題內容要跳轉的連結
    	          left: title_x[i]+'%',
    	          top:   title_y[i]+'%',
    	          textAlign: 'center',
 	    	}; 	

 	    	 
 	    	var pass = row.answre_count == 0  ? 0 :(row.answre_count / (row.answre_count + row.on_answre_count) * 100 ).toFixed(2);
			 

 	        var nopass= row.on_answre_count == 0 ? 0 :  (100 - pass).toFixed(2);
 	        
 	    	/**
	    	 
        	45ba59 綠色
	        FFE153 黃色
            ee6d61 紅色
            ccc   底色
           */
			var color = "#FF5151";
 	    	if (pass >= 80) {
 	    		color = "#45ba59";
 	 	 	} else if (pass >= 60 && pass < 80 ) {
 	 	 		color = "#FFE153";
 	 	 	}
 	    	
 	    	datas[i] ={
			   type: 'pie',
    		   radius: ['15%', '25%'],
  		       center: [data_x[i] + '%', data_y[i] + '%'],
    		   
       		   data: [
          		   {area_tyep :row.area_tyep , value: pass ,   answre_count : row.answre_count , name: '合格' , score: row.pass ,  itemStyle:{color:color} , url:row.url+"&type=1"},
 		           {area_tyep :row.area_tyep , value: nopass , answre_count : row.on_answre_count ,   name: '不合格' , score:row.nopass , itemStyle:{color:"#ccc"},  url:row.url+"&type=2"},
     		   ],

     		   label: {
      			  show: true,
   			      position: 'top',
     			  backgroundColor: '#F6F8FC',
     	   	        borderColor: '#8C8D8E',
     	   	        borderWidth: 1,
     	   	        borderRadius: 4,
        	        rich: {
        	            a: {
        	              color: '#6E7079',
        	              lineHeight: 22,
        	              align: 'center',
        	              fontSize: 14,
        	              fontWeight: 'bold',
        	            },
        	            hr: {
        	              borderColor: '#8C8D8E',
        	              width: '100%',
        	              borderWidth: 1,
        	              height: 0
        	            },
        	            b: {
        	              color: '#4C5058',
        	              fontSize: 14,
        	              align: 'left',
        	              lineHeight: 26
        	            },
        	            
        	          },
      			  formatter: function (datas) {
  	    	         var data = datas.data
  	    	         return  "{a|"+data.name + "("+data.value+")%}"+
  	    	               "\n{hr|}"+
  	    	               "\n{b|店家數 :" + data.answre_count + "}"
  	    	       } 
    		   },
 	    	};
 	 	}

  	    
        
        // based on prepared DOM, initialize echarts instance
          myChart = echarts.init(document.getElementById('main'));

        
        
        
        var  option = {
      		  
      		  
          	  title:  title,
              series : datas
          	  /*
      		  series: [
      		    {
      		      type: 'pie',
        		  radius: ['20%', '30%'],
      		      center: ['20%', '30%'],
      		      
           		  data: [
              		   { value: 1000, name: '合格',  itemStyle:{color:"#45ba59"}},
     		           { value: 200, name: '不合格' , itemStyle:{color:"#ccc"}},
         		  ],
          		},
      		    {
      		      type: 'pie',
        		  radius: ['20%', '30%'],
      		      center: ['40%', '30%'],
        		  data: [
       		    	    { value: 1048, name: '合格',  itemStyle:{color:"#ee6d61"}},
       		            { value: 735, name: '不合格' , itemStyle:{color:"#ccc"}},
         		  ],
          		 

          		},
      		    {
      		      type: 'pie',
        		  radius: ['20%', '30%'],
      		      center: ['60%', '30%'],
        		    data: [
     		    	   { value: 1048, name: '合格',  itemStyle:{color:"#FFE153"}},
     		           { value: 735, name: '不合格' , itemStyle:{color:"#ccc"}},
         		      ],
         		       
          		    
      		    },
      		    {
      		      type: 'pie',
        		  radius: ['20%', '30%'],
      		      center: ['80%', '30%'],
        		  data: [
       			   { value: 1048, name: '合格',  itemStyle:{color:"#FFE153"}},
 		           { value: 735, name: '不合格' , itemStyle:{color:"#ccc"}},
         		  ],
      		    },
      		    
        		{
      		      type: 'pie',
        		  radius: ['20%', '30%'],
      		      center: ['20%', '75%'],
        		  data: [
       			   { value: 1048, name: '合格',  itemStyle:{color:"#FFE153"}},
 		           { value: 735, name: '不合格' , itemStyle:{color:"#ccc"}},
         		  ],
      		    }
      		  ]*/
      		};

        // use configuration item and data specified to show chart
        myChart.setOption(option);
        myChart.on('click', function (params) {
            var index = -1;
  			console.log(params);

  			
            switch(params.data.area_tyep) {
                case '全國':
                    index =0;
                    break;
                case '北一區':
                    index =1;
                    break;
                case '北二區':
                    index =2;
                    break;
                case '中區':
                    index =3;
                    break;
                case '南區':
                    index =4;
                    break;
            }	

            window.location = params.data.url;
			/*
        	if (index != -1) {  
       		    $('input:radio[name=area_tyep]').filter('[value='+index+']').prop('checked', true);
        		$("button[type=submit]").trigger( "click" );
        	}*/
        });
        
    });
</script>
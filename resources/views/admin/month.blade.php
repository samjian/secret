<!-- prepare a DOM container with width and height -->
<div id="main" style="width: 100%;height:800px;"></div>
<script type="text/javascript">
	var myChart;
	window.onresize = function(event) {
		if (myChart) {
    		myChart.resize();
		}
    }

    $(function () {

 		var rows = {!! json_encode($datas) !!};

   
		var rows_arr = $.map( rows, function( row ) {
	    	  return  row ;
	    });

	      
		var cname = rows_arr.length > 0 ?  rows_arr[0].name : "";
 
	    var dates = $.map( rows, function( row ) {
	    	  return  row.gdate ;
	    });

	    var on_answre_count_sum = 0;
	    var answre_count_sum =0;
	 
	    var scoreNoPass = $.map( rows, function( row ) {
	    	  return  row.nopass ;
	    });

	    var on_answre_count = $.map( rows, function( row ) {
	    	on_answre_count_sum+= row.on_answre_count;
	    	  return  row.on_answre_count ;
	    });


	    var answre_count = $.map( rows, function( row ) {
	    	  answre_count_sum+= row.answre_count ;
	    	  return  row.answre_count ;
	    });
	    
		 
 
        myChart = echarts.init(document.getElementById('main'));
        
        var  option =  {
       		  title: {
 			    text: cname,
  			    subtext: '總資料',
    			  subtextStyle : {
    	  	            color :'#6E7079',
    	  	            fontStyle :'bold',
    	  	            fontWeight :'14',
    	  	            fontSize :'12'
    	  	        },       
  			    left: 'center'
 			  },
      		 /*
      		  hover: {
  			    onHover: function(e) {
  			      $("#id-of-your-canvas").css("cursor", e[0] ? "pointer" : "default");
  			    }
  			  },*/
  			  tooltip: {
    				trigger: 'axis',
   				 
    			    formatter: function (datas, i) {
        			       
        			      var n  = datas[0].dataIndex;
        			       // console.log(datas);
        			      //rows = rows_arr[n];
 
		    	          return  name +
		    	          	   "<br>日期 :" + dates[n] + 
		    	       		   "<br>合格率 :" + datas[1].value.toFixed(2) + "%" + 
			    	       	   "<br>不合格率 :" + datas[0].value.toFixed(2) + "%" + 
		    	               "<br>合格店家數 :" + answre_count[n] +
		    	               "<br>不合格店家 :" + on_answre_count[n] 
    			    }
    		  },
  			  legend: {
   				 	left: 'left',
    			    data: ['不合格', '合格']
    			  },
  			  xAxis: [
  			    {
  			      type: 'category',
  			      data: dates
  			    
  			    }
  			  ],
  			  yAxis: {
   				  type: 'value',   
				  min: 0,
	   		      max: 100,
	   		      interval: 10,
	   		      axisLabel: {
	   		        formatter: '{value}%'
	   		      }
    		  },

    		  
    		  grid: { top: '40%' },
  			  series: [
  				  {
    			        type: 'pie',
    			        radius: '25%',
    			        center: ['50%', '22%'],
    			        cursor: 'default',
  					    data: [
 		       			  { value: answre_count_sum, name: '合格',  itemStyle:{color:"#45ba59"}},
	 		              { value: on_answre_count_sum, name: '不合格' , itemStyle:{color:"#ee6d61"}},
	         		    ],
		         		  
		         		  
             	         label: {
             	        	 show: true,
              	        	 rich: {
                	            a: {
                	              color: '#6E7079',
                	              lineHeight: 22,
                	              align: 'center',
                	              fontSize: 12,
                	              fontWeight: 'bold',
                	            },
                	          },
	             	        formatter: '{a|{b} : 店家數 {@2012} ({d}%)}'
	             	     },
			         		   
  				},
  			    {
    			   name: '不合格',
  			       type: 'bar',
    			   barWidth: 30,
    			   cursor: 'default',
    			   itemStyle:{
    				   color:"#ee6d61"
  				  },
  			      data: $.map( rows, function( row ) {
  			    	  return  row.on_answre_rate ;
    			  })
    		        
  			    },
			    {
			      name: '合格',
			      type: 'bar',
			      cursor: 'default',
			      itemStyle:{
				      color:"#45ba59"
				  },
			      barWidth: 30,
			      data:  $.map( rows, function( row ) {
			    	  return  row.answre_rate ;
				  })
			    },
  			  ],
  			  
  			};


        // use configuration item and data specified to show chart
        myChart.setOption(option );
        
    });
</script>